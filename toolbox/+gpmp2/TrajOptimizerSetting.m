%class TrajOptimizerSetting, see Doxygen page for details
%at http://research.cc.gatech.edu/borg/sites/edu.borg/html/index.html
%
%-------Constructors-------
%TrajOptimizerSetting(size_t dof)
%
%-------Methods-------
%setDogleg() : returns void
%setGaussNewton() : returns void
%setLM() : returns void
%set_Qc_model(Matrix Qc) : returns void
%set_conf_prior_model(double sigma) : returns void
%set_cost_sigma(double sigma) : returns void
%set_epsilon(double eps) : returns void
%set_max_iter(size_t iter) : returns void
%set_obs_check_inter(size_t inter) : returns void
%set_rel_thresh(double thresh) : returns void
%set_total_step(size_t step) : returns void
%set_total_time(double time) : returns void
%set_vel_prior_model(double sigma) : returns void
%
classdef TrajOptimizerSetting < handle
  properties
    ptr_gpmp2TrajOptimizerSetting = 0
  end
  methods
    function obj = TrajOptimizerSetting(varargin)
      if nargin == 2 && isa(varargin{1}, 'uint64') && varargin{1} == uint64(5139824614673773682)
        my_ptr = varargin{2};
        gpmp2_wrapper(98, my_ptr);
      elseif nargin == 1 && isa(varargin{1},'numeric')
        my_ptr = gpmp2_wrapper(99, varargin{1});
      else
        error('Arguments do not match any overload of gpmp2.TrajOptimizerSetting constructor');
      end
      obj.ptr_gpmp2TrajOptimizerSetting = my_ptr;
    end

    function delete(obj)
      gpmp2_wrapper(100, obj.ptr_gpmp2TrajOptimizerSetting);
    end

    function display(obj), obj.print(''); end
    %DISPLAY Calls print on the object
    function disp(obj), obj.display; end
    %DISP Calls print on the object
    function varargout = setDogleg(this, varargin)
      % SETDOGLEG usage: setDogleg() : returns void
      % Doxygen can be found at http://research.cc.gatech.edu/borg/sites/edu.borg/html/index.html
      gpmp2_wrapper(101, this, varargin{:});
    end

    function varargout = setGaussNewton(this, varargin)
      % SETGAUSSNEWTON usage: setGaussNewton() : returns void
      % Doxygen can be found at http://research.cc.gatech.edu/borg/sites/edu.borg/html/index.html
      gpmp2_wrapper(102, this, varargin{:});
    end

    function varargout = setLM(this, varargin)
      % SETLM usage: setLM() : returns void
      % Doxygen can be found at http://research.cc.gatech.edu/borg/sites/edu.borg/html/index.html
      gpmp2_wrapper(103, this, varargin{:});
    end

    function varargout = set_Qc_model(this, varargin)
      % SET_QC_MODEL usage: set_Qc_model(Matrix Qc) : returns void
      % Doxygen can be found at http://research.cc.gatech.edu/borg/sites/edu.borg/html/index.html
      if length(varargin) == 1 && isa(varargin{1},'double')
        gpmp2_wrapper(104, this, varargin{:});
      else
        error('Arguments do not match any overload of function gpmp2.TrajOptimizerSetting.set_Qc_model');
      end
    end

    function varargout = set_conf_prior_model(this, varargin)
      % SET_CONF_PRIOR_MODEL usage: set_conf_prior_model(double sigma) : returns void
      % Doxygen can be found at http://research.cc.gatech.edu/borg/sites/edu.borg/html/index.html
      gpmp2_wrapper(105, this, varargin{:});
    end

    function varargout = set_cost_sigma(this, varargin)
      % SET_COST_SIGMA usage: set_cost_sigma(double sigma) : returns void
      % Doxygen can be found at http://research.cc.gatech.edu/borg/sites/edu.borg/html/index.html
      gpmp2_wrapper(106, this, varargin{:});
    end

    function varargout = set_epsilon(this, varargin)
      % SET_EPSILON usage: set_epsilon(double eps) : returns void
      % Doxygen can be found at http://research.cc.gatech.edu/borg/sites/edu.borg/html/index.html
      gpmp2_wrapper(107, this, varargin{:});
    end

    function varargout = set_max_iter(this, varargin)
      % SET_MAX_ITER usage: set_max_iter(size_t iter) : returns void
      % Doxygen can be found at http://research.cc.gatech.edu/borg/sites/edu.borg/html/index.html
      gpmp2_wrapper(108, this, varargin{:});
    end

    function varargout = set_obs_check_inter(this, varargin)
      % SET_OBS_CHECK_INTER usage: set_obs_check_inter(size_t inter) : returns void
      % Doxygen can be found at http://research.cc.gatech.edu/borg/sites/edu.borg/html/index.html
      gpmp2_wrapper(109, this, varargin{:});
    end

    function varargout = set_rel_thresh(this, varargin)
      % SET_REL_THRESH usage: set_rel_thresh(double thresh) : returns void
      % Doxygen can be found at http://research.cc.gatech.edu/borg/sites/edu.borg/html/index.html
      gpmp2_wrapper(110, this, varargin{:});
    end

    function varargout = set_total_step(this, varargin)
      % SET_TOTAL_STEP usage: set_total_step(size_t step) : returns void
      % Doxygen can be found at http://research.cc.gatech.edu/borg/sites/edu.borg/html/index.html
      gpmp2_wrapper(111, this, varargin{:});
    end

    function varargout = set_total_time(this, varargin)
      % SET_TOTAL_TIME usage: set_total_time(double time) : returns void
      % Doxygen can be found at http://research.cc.gatech.edu/borg/sites/edu.borg/html/index.html
      gpmp2_wrapper(112, this, varargin{:});
    end

    function varargout = set_vel_prior_model(this, varargin)
      % SET_VEL_PRIOR_MODEL usage: set_vel_prior_model(double sigma) : returns void
      % Doxygen can be found at http://research.cc.gatech.edu/borg/sites/edu.borg/html/index.html
      gpmp2_wrapper(113, this, varargin{:});
    end

  end

  methods(Static = true)
  end
end
