function [ H, error ] = getMAPTrajectory( G, graph, result )
%GETMAPTRAJECTORY 
%
%   First node is start.
%   Last node is goal.
%
%   Return matlab.graph

%% 
import gtsam.*
import gpmp2.*
import graph.*

% Copy results to graph.
G = copyResultsToGraph(G, result);
V = G.Nodes;

% Graph size.
num_nodes = size(V,1);

% Aggregate factor costs into graph.
pos = V.Pos;
vel = V.Vel;
t = V.Time;
U(num_nodes) = MAPNode;
for i = 0:graph.nrFactors-1
    factor = graph.at(i);
    if isa(factor, 'ObstacleSDFFactorGPArm')
        % Get key indexes.
        key = factor.keys();
        k1 = symbolIndex(key.at(0));
        k2 = symbolIndex(key.at(2));
        assert(k1 ~= k2)
        % Sort by time.
        if t(k2) < t(k1)
            [k2, k1] = deal(k1, k2);
        end
        % Compute costs.
        w = factor.error(result);
        % Add edge.
        u = U(k1);
        u.k = k1;
        e = u.e([u.e.k] == k2);
        if isempty(e)
            e = MAPEdge;
            e.k = k2;
            u.e(end+1) = e;
        end
        e.w = e.w + w;
        e.w_obs = e.w_obs + w;
    end
    if isa(factor, 'ObstacleSDFFactorArm')
        % Get key indexes.
        key = factor.keys();
        k = symbolIndex(key.at(0));
        % Compute costs.
        w = factor.error(result);
        % Add cost to node.
        u = U(k);
        u.k = k;
        u.w = w;
        u.t = t(k);
        u.pos = pos(k,:)';
        u.vel = vel(k,:)';
    end
    if isa(factor, 'GaussianProcessPriorLinear')
        % Get key indexes.
        key = factor.keys();
        k1 = symbolIndex(key.at(0));
        k2 = symbolIndex(key.at(2));
        assert(k1 ~= k2)
        % Sort by time.
        if t(k2) < t(k1)
            [k2, k1] = deal(k1, k2);
        end
        % Compute costs.
        w = factor.error(result);
        % Add cost to edge.
        u = U(k1);
        e = u.e([u.e.k] == k2);
        if isempty(e)
            e = MAPEdge;
            e.k = k2;
            u.e(end+1) = e;
        end
        e.w = e.w + w;
        e.w_gp = e.w_gp + w;
    end
    if isa(factor, 'PriorFactorVector')
        % Get key indexes.
        key = factor.keys();
        k = symbolIndex(key.at(0));
        % Copy state.
        u = U(k);
        u.t = t(k);
        u.pos = pos(k,:)';
        u.vel = vel(k,:)';
        u.k = k;
    end
end

% Remove start and goal from unexplored.
s = U(1);
g = U(end);

% Search for MAP trajectory.
s.c = 0;
F = MAPHeap(num_nodes);
F.push(s);
while ~F.empty()
    u = F.pop();
    if u == g
        break
    end
    for e = u.e
        v = U(e.k);
        c = u.c + e.w + v.w;
        if c < v.c
            v.c = c;
            v.c_obs = u.c_obs + e.w_obs + v.w;
            v.c_gp = u.c_gp + e.w_gp;
            v.p = u.k;
        end
        F.push(v);
    end
end
map = [num_nodes];
while u ~= s
    map = [u.p map];
    u = U(u.p);
end

% Convert to graph.
A = zeros(length(map));
for i = 1:length(map)-1
    A(i,i+1) = 1;
    A(i+1,i) = 1;
end
H = generateGraph(A, [U(map).pos]', [U(map).vel]', [U(map).t]');
error = U(end).c
gp_error = U(end).c_gp
obs_error = U(end).c_obs
% plotGraph2D(H, gp.Qc_model)

end

