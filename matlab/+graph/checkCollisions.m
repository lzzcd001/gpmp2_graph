function [ col_free ] = checkCollisions( G, sdf, pR_model, Qc_model, dataset )
%CHECKCOLLISIONS 
%   


%% Check collisions.
import gtsam.*
import gpmp2.*
import graph.*

% Obstacle factor settings for collision checking.
epsilon_dist = 0;
cost_sigma = 0.5;

% Get times.
t = G.Nodes.Time;
delta_t = t(2)-t(1);

% Number of interpolated states to check for collisions.
check_inter = 10.0;
check_step = delta_t / check_inter;

% Get nodes and edges.
V = G.Nodes;
E = G.Edges.Pairs;

% Check for collisions.
num_edges = size(E, 1);
rm_edges = [];
% Obstacle cost.
obs = ObstaclePlanarSDFFactorGPPointRobot( ...
    symbol('x',1), symbol('v',1), symbol('x',2), symbol('v',2), ...
    pR_model, sdf, cost_sigma, epsilon_dist, ...
    Qc_model, delta_t, 0);
for i = 1:num_edges
    % Find indices.
    k = E(i,:);
    % Get end-point states.
    x1 = V.Pos(k(1),:)';
    v1 = V.Vel(k(1),:)';
    x2 = V.Pos(k(2),:)';
    v2 = V.Vel(k(2),:)';
    % Check interpolation for collisions.
    for j = 0:check_inter
        tau = j * check_step;
        % Obstacle cost.
        obs.setTau(tau);
        err = obs.evaluateError(x1, v1, x2, v2);
        % If in collision, remove edge from graph.
        if err > 0
            rm_edges = [rm_edges i]; %#ok<AGROW>
            break
        end
    end
end
G = rmedge(G, rm_edges);

% Test if graph is fully connected (=> collision free path).
num_nodes = size(V, 1);
bins = conncomp(G);
col_free = bins(1) == bins(num_nodes);

end

