function [ result, elapsedTime ] = gtsamOptimize( graph, init_values )
%GTSAMOPTIMIZE 
%   Need custom iterate because GTSAM optimize exits on solution with 
%   larger error.

%% 
import gtsam.*

use_trustregion_opt = false;

if use_trustregion_opt
    parameters = DoglegParams;
    parameters.setVerbosity('ERROR');
    optimizer = DoglegOptimizer(graph, init_values, parameters);
else
    parameters = GaussNewtonParams;
    parameters.setVerbosity('ERROR');
    optimizer = GaussNewtonOptimizer(graph, init_values, parameters);
end

% Optimize.
max_iter = parameters.getMaxIterations;
abs_tol = parameters.getAbsoluteErrorTol;
rel_tol = parameters.getRelativeErrorTol;
best_values = init_values;
best_err = graph.error(init_values);
tic
for i = 1:max_iter
    optimizer.iterate();
    values = optimizer.values();
    err = graph.error(values);
    % compare error
    if err < best_err
        best_values = values;
        best_err = err;
    end
    abs = best_err - err;
    rel = abs / err;
    if abs < abs_tol || rel < rel_tol
        break
    end
end
elapsedTime = toc;
format compact
result = best_values;

end

