function [ dataset, x0, xN ] = generateUniformMazeDataset( num_rows, num_cols )
%GENERATEUNIFORMMAZEDATASET
%   

%% Uniformly sample a maze.
import graph.*
import gtsam.*
import gpmp2.*

% Grid parameters.
% num_rows = 3;
% num_cols = 3;
origin = [-10, -10];
h = 6 * (num_rows);
w = 6 * (num_cols);
% assert(h/w == num_rows/num_cols);

% Dataset parameters.
path_width = 0.50;
cell_size = 0.1;

% Create grid.
clear V
V(num_rows, num_cols) = WilsonNode;
h_step = h/(num_rows); % Leave half step on either side.
w_step = w/(num_cols); % 
o_x = origin(1) + w_step / 2;
o_y = origin(2) + h_step / 2;
x0 = [o_x, o_y]';
xN = [o_x + (num_cols-1)*w_step, o_y + (num_rows-1)*h_step]';
for i = 1:num_rows
    for j = 1:num_cols
        % Node positions.
        V(i,j).x = o_x + (j-1)*w_step;
        V(i,j).y = o_y + (i-1)*h_step;
        % Edges.
        ii = i + (-1:2:1)';
        jj = j + (-1:2:1)';
        ii = ii(and(0 < ii, ii <= num_rows));
        jj = jj(and(0 < jj, jj <= num_cols));
        V(i,j).e = [ii repmat(j, [length(ii), 1]); ...
                repmat(i, [length(jj), 1]), jj];
    end
end

% Wilson's algorithm.
num_nodes = length(V(:));
i = randi(num_nodes);
V(i).s = 1;
U = V([V.s] ~= 1);
while ~isempty(U)
    % Random walk.
    u = U(randi(length(U)));
    i = 1;
    u.t = i;
    u.n = u.e(randi(length(u.e)), :);
    g = V(u.n(1), u.n(2));
    assert(~u.s)
    while ~g.s
        i = i+1;
        g.t = i;
        g.n = g.e(randi(length(g.e)), :);
        g = V(g.n(1), g.n(2));
    end
    % Loop erasure.
    g = u;
    i = 1;
    while ~g.s
        g.t = i;
        g = V(g.n(1), g.n(2));
        i = i + 1;
    end
    dt = abs(mod(i,2)-g.t);
    % Add to spanning tree.
    g = u;
    while ~g.s
        g.s = 1;
        g.t = mod(g.t + dt, 2);
        g = V(g.n(1), g.n(2));
    end
    U = U([U.s] ~= 1);
end

% % Create graph transition matrix.
% A = zeros(num_nodes);
% Pos = zeros([num_nodes, 2]);
% for i = 1:num_rows
%     for j = 1:num_cols
%         v = V(i, j);
%         % position
%         ii = sub2ind(size(V), i, j);
%         Pos(ii,:) = [v.x v.y];
%         % edge
%         if ~isempty(v.n)
%             jj = sub2ind(size(V), v.n(1), v.n(2));
%             A(ii,jj) = 1;
%             A(jj,ii) = 1;
%         end
%     end
% end
% assert(sum(diag(A)) == 0)
% 
% % Create graph.
% G = graph(A);
% G.Nodes.Pos = Pos;
% G.Nodes.Vel = zeros([num_nodes, 2]);
% G.Nodes.Time = [V.t]';
% 
% % Plot.
% Qc = eye(2);
% Qc_model = noiseModel.Gaussian.Covariance(Qc);
% clf();
% plotGraph2D(G, Qc_model);
% axis equal

% Create obstacles.
cell_rows = h / cell_size;
cell_cols = w / cell_size;
map = ones(cell_rows, cell_cols);
for i = 1:num_rows
    for j = 1:num_cols
        if isempty(V(i,j).n)
            continue
        end
        s = V(i,j);
        t = V(s.n(1), s.n(2));
        tr_x = max(s.x, t.x) + w_step*path_width/2;
        tr_y = max(s.y, t.y) + h_step*path_width/2;
        bl_x = min(s.x, t.x) - w_step*path_width/2;
        bl_y = min(s.y, t.y) - h_step*path_width/2;
        tr = [tr_y - origin(2), tr_x - origin(1)]./cell_size;
        bl = [bl_y - origin(2), bl_x - origin(1)]./cell_size;
        tr = round(tr);
        bl = round(bl);
        map(bl(1):tr(1), bl(2):tr(2)) = 0;
    end
end

% Create dataset.
dataset.cols = cell_cols; %x
dataset.rows = cell_rows; %y
dataset.origin_x = origin(1);
dataset.origin_y = origin(2);
dataset.cell_size = cell_size;
dataset.map = map;

% Plot evidence.
% hold on
% plotEvidenceMap2D(dataset.map, dataset.origin_x, dataset.origin_y, cell_size);

end
