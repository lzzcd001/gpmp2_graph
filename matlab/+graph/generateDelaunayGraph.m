function [ G ] = generateDelaunayGraph( start_conf, end_conf, avg_vel, dataset, n_sample )
%GENERATEDELANEYGRAPH Sample Delaunay graph.
%   

%% Sample random points and create Delaunay triangulation.

% Get dimensions.
h = dataset.rows * dataset.cell_size;
w = dataset.cols * dataset.cell_size;
o_x = dataset.origin_x;
o_y = dataset.origin_y;

% Sample n points.
x = o_x + w * rand([n_sample, 1]);
y = o_y + h * rand([n_sample, 1]);
x = [start_conf(1); x; end_conf(1)];
y = [start_conf(2); y; end_conf(2)];

% Total number of points.
N = n_sample + 2;

% Compute Delaunay triangulation.
tri = delaunay(x,y);
% triplot(tri,x,y)

%% Convert triangulation to graph.

% Create edges.
A = zeros(N);
for i = 1:length(tri)
    t = sort(tri(i,:));
    A(t(1),t(2)) = 1;
    A(t(2),t(3)) = 1;
    A(t(1),t(3)) = 1;
end
A = A + A';
G = graph(A);

% Create nodes.
names = cell([N,1]);
for i = 1:N
    names{i} = ['x' num2str(i, '%d')];
end
G.Nodes.Name = names;
G.Nodes.Pos = [x y];
G.Nodes.Vel = repmat(avg_vel', [N 1]);


end

