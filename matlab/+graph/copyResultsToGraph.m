function [ G ] = copyResultsToGraph( G, results, graph )
%CONVERTFACTORGRAPHTOGRAPH 
%   
import gtsam.*

%% Copy results of optimization over to graph.
V = G.Nodes;
num_nodes = size(G.Nodes,1);
for i = 1:num_nodes
    V.Pos(i,:) = results.atVector(symbol('x', i));
    V.Vel(i,:) = results.atVector(symbol('v', i));
end
G.Nodes = V;

% TODO Copy over optimization values (like cost at node and edge path cost)

end

