function [ dataset ] = generatePegDataset( num_rows, num_cols )
%GENERATEPEGDATASET Generate peg board dataset
%
%   Usage: dataset = GENERATE2DDATASET(dataset_str)
%   @dataset_str       dataset string, existing datasets:
%                      'OneObstacleDataset', 'TwoObstaclesDataset'
%
%   Output Format:
%   dataset.map        ground truth evidence grid
%   dataset.rows       number of rows (y)
%   dataset.cols       number of cols (x)
%   dataset.origin_x   origin of map x
%   dataset.origin_y   origin of map y
%   dataset.cell_size  cell size

%% 

% params
dataset.cols = 400; %x
dataset.rows = 300; %y
dataset.origin_x = -20;
dataset.origin_y = -10;
dataset.cell_size = 0.1;
% map
dataset.map = zeros(dataset.rows, dataset.cols);
% obstacles

%% Generate peg locations.

% Peg board height and width.
h = dataset.rows * dataset.cell_size;
w = dataset.cols * dataset.cell_size;

% Peg spacing and size.
% num_rows = 1;
% num_cols = 1;
step_vert = h / (num_rows); % Leave half step on either side.
step_horz = w / (num_cols); % 
peg_diam = round(0.3 * min(step_vert, step_horz));
peg_diam = cast(peg_diam, 'int32');

% Bottom left peg.
blp_x = dataset.origin_x + step_horz / 2;
blp_y = dataset.origin_y + step_vert / 2;

% Peg locations.
pegs = cell([num_rows * num_cols, 1]);
p_i = 1;
for i = 1:num_rows
    for j = 1:num_cols
        if mod(i-1,2) == 1 && j == num_cols
            continue
        end
        px = blp_x + (j-1)*step_horz + mod(i-1,2)*step_horz/2;
        py = blp_y + (i-1)*step_vert;
        pegs{p_i} = [py, px];
        p_i = p_i+1;
    end
end
pegs = pegs(1:p_i-1);

% Add pegs.
for i = 1:length(pegs)
    x = pegs{i}(2);
    y = pegs{i}(1);
    [ix, iy] = get_center(x,y,dataset);
    dataset.map = add_obstacle([iy, ix], ...
        get_dim(peg_diam, peg_diam, dataset), dataset.map);
end


%%
% dataset.map = add_obstacle(get_center(12,10,dataset), get_dim(5,7,dataset), dataset.map);
% dataset.map = add_obstacle(get_center(-7,10,dataset), get_dim(10,7,dataset), dataset.map);
% dataset.map = add_obstacle(get_center(0,-5,dataset), get_dim(10,5,dataset), dataset.map);

end


%
function [map, landmarks] = add_obstacle(position, size, map, landmarks, origin_x, origin_y, cell_size)

half_size_row = floor((size(1)-1)/2);
half_size_col = floor((size(2)-1)/2);

% occupency grid
map(position(1)-half_size_row : position(1)+half_size_row, ...
    position(2)-half_size_col : position(2)+half_size_col) ...
    = ones(2*half_size_row+1, 2*half_size_col+1); 

% landmarks
if nargin == 7
    for x = position(1)-half_size_row-1 : 4 : position(1)+half_size_row-1
        y = position(2)-half_size_col-1;
        landmarks = [landmarks; origin_y+y*cell_size, origin_x+x*cell_size];
        y = position(2)+half_size_col-1;
        landmarks = [landmarks; origin_y+y*cell_size, origin_x+x*cell_size];
    end
    
    for y = position(2)-half_size_col+3 : 4 : position(2)+half_size_col-5
        x = position(1)-half_size_row-1;
        landmarks = [landmarks; origin_y+y*cell_size, origin_x+x*cell_size];
        x = position(1)+half_size_row-1;
        landmarks = [landmarks; origin_y+y*cell_size, origin_x+x*cell_size];
    end
end

end

function [cx, cy] = get_center(x,y,dataset)

center = [y - dataset.origin_y, x - dataset.origin_x]./dataset.cell_size;
cx = center(2);
cy = center(1);

end

function dim = get_dim(w,h,dataset)

dim = [h, w]./dataset.cell_size;

end