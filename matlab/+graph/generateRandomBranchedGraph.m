function [ G ] = generateRandomBranchedGraph( n_random, n_branches, gp )
%GENERATERANDOMBRANCHEDGRAPH 
%   

%% Generate branched graph with random edge connections.
import graph.*

% Get branched graph.
G = generateBranchedGraph(n_branches, gp);

% Create potential edge list.
N = length(gp.t);
e = zeros([(n_branches-1)*(N-3)*2, 2]);
c = 1;
for i = 1:n_branches-1;
    for j = 1:N-3;
        for k = -1:2:1
            ii = i;
            if k == -1
                ii = i + 1;
            end
            s = 1 + (ii-1)*(N-2) + j;
            t = 1 + (ii-1+k)*(N-2) + j + 1;
            e(c,:) = [s t];
            assert(c <= size(e,1));
            c = c+1;
        end
    end
end
assert(c-1 == size(e,1));
num_edges = size(e, 1);

% Create edge table.
P = randperm(num_edges, n_random);
V = G.Nodes;
edges = table([V.Name(e(P,1)) V.Name(e(P,2))], ones([n_random, 1]), .... 
    [e(P,1) e(P,2)], 'VariableNames', G.Edges.Properties.VariableNames);
G = addedge(G, edges);

% % Add edges. TOO SLOW
% for r = 1:n_random
%     % Pick pair of branches to connect.
%     i = randi(n_branches-1);
%     % Pick timestamps to connect.
%     j = randi(N-3);
%     % Pick direction.
%     k = (-1)^(randi(2));
%     if k == -1
%         i = i + 1;
%     end
%     % Add edge.
%     s = 1 + (i-1)*(N-2) + j;
%     t = 1 + (i-1+k)*(N-2) + j + 1;
%     if ~findedge(G, s, t)
%         e = table([V.Name(s) V.Name(t)], 1, [s t], 'VariableNames', G.Edges.Properties.VariableNames);
%         G = addedge(G, e);
%     end
% end

end

