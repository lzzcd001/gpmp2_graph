function [ dataset ] = generateRandomPegsDataset( obs_percent )
%GENERATERANDOMPEGSDATASET Generate peg board dataset
%
%   Usage: dataset = GENERATE2DDATASET(dataset_str)
%   @dataset_str       dataset string, existing datasets:
%                      'OneObstacleDataset', 'TwoObstaclesDataset'
%
%   Output Format:
%   dataset.map        ground truth evidence grid
%   dataset.rows       number of rows (y)
%   dataset.cols       number of cols (x)
%   dataset.origin_x   origin of map x
%   dataset.origin_y   origin of map y
%   dataset.cell_size  cell size

%% 

% params
dataset.cols = 400; %x
dataset.rows = 300; %y
dataset.origin_x = -20;
dataset.origin_y = -10;
dataset.cell_size = 0.1;
% map
dataset.map = zeros(dataset.rows, dataset.cols);
% obstacles

%% Generate peg locations.

% Peg board height and width.
h = dataset.rows * dataset.cell_size;
w = dataset.cols * dataset.cell_size;

% Get max peg diameter.
max_peg_diam = 0.3 * min(h,w);

% Add random pegs.
total_cells = dataset.cols * dataset.rows;
obs_cells = 0;
while obs_cells/total_cells < obs_percent
    % Sample peg center.
    px = dataset.origin_x + rand*w;
    py = dataset.origin_y + rand*h;
    % Sample peg diameter.
    dh = int32(ceil((rand/2 + 0.5)*max_peg_diam));
    dw = int32(ceil((rand/2 + 0.5)*max_peg_diam));
    % Add peg.
    [dataset.map, delta_obs] = ...
        add_obstacle(get_center(px,py,dataset), get_dim(dw, dh, dataset), ...
            dataset.map);
    % Update obstacle cell count.
    obs_cells = obs_cells + delta_obs;
end
assert(obs_cells == sum(sum(dataset.map)));

end

%
function [map, df] = add_obstacle(position, obs_size, map)

% map size
r = size(map, 1);
c = size(map, 2);

% half size
half_size_row = floor((obs_size(1)-1)/2);
half_size_col = floor((obs_size(2)-1)/2);

% indexes
ii = position(1)-half_size_row : position(1)+half_size_row;
jj = position(2)-half_size_col : position(2)+half_size_col;

% clamp
ii = ii(0 < ii & ii <= r);
jj = jj(0 < jj & jj <= c);

% pre-fill count
prev_count = sum(sum(map(ii,jj)));

% occupency grid
map(ii, jj) = ones(length(ii), length(jj));

% post-fill delta
next_count = sum(sum(map(ii,jj)));
df = next_count - prev_count;

end

function center = get_center(x,y,dataset)

center = [y - dataset.origin_y, x - dataset.origin_x]./dataset.cell_size;

end

function dim = get_dim(w,h,dataset)

dim = [h, w]./dataset.cell_size;

end

