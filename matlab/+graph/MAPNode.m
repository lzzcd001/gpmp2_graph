classdef MAPNode < handle
    %MAPNODE 
    %   
    
    properties
        pos = [0 0];
        vel = [0 0];
        d = 0;
        w = 0;
        k = 0;
        t = 0;
        c = inf;
        c_obs = 0;
        c_gp = 0;
        h = 0;
        p = 0;
        e = graph.MAPEdge;
    end
    
    methods
        function obj = MAPNode()
            f = graph.MAPEdge;
            obj.e = f([]);
        end
    end
    
end

