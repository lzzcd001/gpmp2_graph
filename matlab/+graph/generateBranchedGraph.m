function [ G ] = generateBranchedGraph( n_branches, gp )
%GENERATEBRANCHEDGRAPH 
%   

%% Create branches.
% Get configurations.
t = gp.t;
x0 = [gp.x0; gp.vel];
xN = [gp.xN; gp.vel];

% Get Qc.
R_qc = gp.Qc_model.R();
Qc = inv(R_qc' * R_qc);
% Qc = diag([Qc; Qc]);

% Get P0.
R_pos = gp.pose_fix.R();
P0_pos = diag(inv(R_pos' * R_pos));
R_vel = gp.vel_fix.R();
P0_vel = diag(inv(R_vel' * R_vel));
P0 = diag([P0_pos; P0_vel]);

% Get covariance conditioned on start and end.
[x_bar, K] = gp_cond1N(t, x0, xN, Qc, P0);
K_diag = diag(K);

% Generate upper and lower covariance bounds.
dim = length(P0);
N = length(t);
x_bar = [x0 reshape(x_bar, [dim N-2]) xN];
x0_sigma = zeros([dim, 1]);
xN_sigma = zeros([dim, 1]);
sigma = [x0_sigma reshape(K_diag, [dim N-2]) xN_sigma];

% Plot branches.
% plot(x(1,:), x(2,:), 'b--')
% hold on
% for i = 0:0
%     for j = -3:3
%         x_branch = x + repmat([i; j; i; j;], [1 N]) .* sigma;
%         plot(x_branch(1,:), x_branch(2,:))
%     end
% end

%% Create graph.
% Total number of nodes.
num_nodes = 2 + n_branches*(N - 2);

% Transition matrix.
A = zeros(num_nodes);

% Start node to branches.
for i = 1:n_branches
    A(1, 2 + (i-1)*(N-2)) = 1;
end
% Branches to end node.
for i = 1:n_branches
    A(1 + i*(N-2), end) = 1;
end
% Branch edges.
for i = 1:n_branches
    for j = 1:N-3
        A(2+(i-1)*(N-2)+j-1, 2+(i-1)*(N-2)+j) = 1;
    end
end
A = A + A';

% Node positions.
sigma_range = linspace(-3, 3, n_branches);
pdim = length(P0_pos);
vdim = length(P0_vel);
pos = [gp.x0'; zeros([num_nodes-2 pdim]); gp.xN'];
vel = [gp.vel'; zeros([num_nodes-2 vdim]); gp.vel'];
time = [0; zeros([num_nodes-2 1]); t(end)];
ii_p = 1:pdim;
ii_v = pdim+1:pdim+vdim;
for i = 1:n_branches
    for j = 2:N-1
        % Compute normal vector.
        v = x_bar(ii_v, j);
        v = v./norm(v);
%         R = [cos(pi/2) -sin(pi/2); sin(pi/2) cos(pi/2)];
%         n = R * v;

        % Random normal vector
        n = normrnd(0, 1, [7,1]);

        % Interpolate new positions and velocities.
        s = sigma_range(i);
        p_t = x_bar(ii_p, j) + s * norm(sigma(ii_p,j)) .* n;
        v_t = x_bar(ii_v, j) + abs(s) * norm(sigma(ii_v,j)) .* v;
        % Copy to pos/vel vectors.
        pos((i-1)*(N-2)+j,:) = p_t';
        vel((i-1)*(N-2)+j,:) = v_t';
        time((i-1)*(N-2)+j) = t(j);
    end
end

%% Create graph structure.
G = graph(A);

% Create nodes.
names = cell([num_nodes,1]);
for i = 1:num_nodes
    names{i} = ['x' num2str(i, '%d')];
end
G.Nodes.Name = names;
G.Nodes.Pos = pos;
G.Nodes.Vel = vel;
G.Nodes.Time = time;

% Store edge indexes for efficiency.
num_edges = size(G.Edges,1);
E = G.Edges.EndNodes;
Pairs = zeros([num_edges, 2]);
for i = 1:num_edges
    s = E{i,1};
    t = E{i,2};
    s = str2double(s(2:end));
    t = str2double(t(2:end));
    Pairs(i,:) = [s t];
end
G.Edges.Pairs = Pairs;

end

