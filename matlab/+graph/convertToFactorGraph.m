function [ graph, init_values ] = convertToFactorGraph( G, gp, robot, dataset, varargin )
%CONVERTTONONLINEARFACTORGRAPH Convert graph into nonlinear factor graph.
% 

%% Handle optional parameters.
p = inputParser;
addParameter(p, 'AddGPPriorLinear', true);
R_qc = gp.Qc_model.R();
Qc = inv(R_qc' * R_qc);
addParameter(p, 'CovGPPriorLinear', Qc(1,1));
parse(p, varargin{:});

%% Convert graph into factors.

import gtsam.*
import gpmp2.*

% Optimization parameters.
pose_fix     = gp.pose_fix;
vel_fix      = gp.vel_fix;
pR_model     = robot.pR_model;
sdf          = dataset.sdf;
cost_sigma   = gp.cost_sigma;
epsilon_dist = gp.epsilon_dist;
Qc_model     = gp.Qc_model;
t            = gp.t;
delta_t      = gp.delta_t;
use_GP_inter = gp.use_GP_inter;
total_time_sec = gp.total_time_sec;
total_time_step = gp.total_time_step;
total_check_step = gp.total_check_step;
check_inter = gp.check_inter;

% Prior covariance.
Qc_prior = p.Results.CovGPPriorLinear * eye(length(Qc_model.R()));
Qc_prior_model = noiseModel.Gaussian.Covariance(Qc_prior);

% Initialize variables.
graph = NonlinearFactorGraph;
init_values = Values;

% Get graph variables.
num_nodes = size(G.Nodes,1);
num_edges = size(G.Edges,1);
V = G.Nodes;
E = G.Edges.EndNodes;

% add nodes.
for i = 1:num_nodes
    % symbols
    key_pos = symbol('x', i);
    key_vel = symbol('v', i);
    % values
    pose = V.Pos(i,:)';
    vel  = V.Vel(i,:)';
    init_values.insert(key_pos, pose);
    init_values.insert(key_vel, vel);
    % priors
    if i==1
        graph.add(PriorFactorVector(key_pos, pose, pose_fix));
        graph.add(PriorFactorVector(key_vel, vel, vel_fix));
    elseif i==num_nodes
        graph.add(PriorFactorVector(key_pos, pose, pose_fix));
        graph.add(PriorFactorVector(key_vel, vel, vel_fix));
    else
        % cost factor
        graph.add(ObstaclePlanarSDFFactorPointRobot(...
            key_pos, pR_model, sdf, cost_sigma, epsilon_dist));
    end
end

% add factors
E = G.Edges.Pairs;
for i = 1:num_edges
    % indexes
    ii = E(i,:);
    % symbols
    key_pos1 = symbol('x', ii(1));
    key_vel1 = symbol('v', ii(1));
    key_pos2 = symbol('x', ii(2));
    key_vel2 = symbol('v', ii(2));
    % factors
    if p.Results.AddGPPriorLinear
        graph.add(GaussianProcessPriorLinear(key_pos1, key_vel1, ...
            key_pos2, key_vel2, delta_t, Qc_prior_model));
    end
    % GP cost factor
    if use_GP_inter && check_inter > 0
        for j = 1:check_inter
            tau = j * (total_time_sec / total_check_step);
            graph.add(ObstaclePlanarSDFFactorGPPointRobot( ...
                key_pos1, key_vel1, key_pos2, key_vel2, ...
                pR_model, sdf, cost_sigma, epsilon_dist, ...
                Qc_model, delta_t, tau));
        end
    end
end


end

