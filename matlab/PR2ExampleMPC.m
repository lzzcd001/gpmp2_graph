% @brief    7DOF PR2 right arm example for GP Simultaneous Planning and Control
% @author   Mustafa Mukadam
% @date     August 24, 2016

%% Load libraries
import gtsam.*
import gpmp2.*
import gpspac.*
clear
clc

%% Settings
delta_t = 0.05; % time between two support states
time_horizon = 1; % receding horizon
intp_states = 10; % number of interpolation states

Qx = 0.01;
Qu = 100;
Qfix = 1e-4;
Qgoal = 0.01;
Qm = 0.01;

Qobs = 0.01;
epsilon_dist = 0.2;

start = [-0.2762, 0.894 , -0.4462, -1.6105, 2.328 , -1.0881, -2.3999]';
goal =  [-0.571 , -0.3534, -1.7684, -1.5384, -2.7693, -1.5537, -1.4818]';

seed = 0;
maxiter = 100;

save_traj = 0;

%% Video output
output_video = false;
result_video_filename = '~/Desktop/result.avi';
if output_video
    vwr = VideoWriter(result_video_filename, 'Motion JPEG AVI');
    vwr.Quality = 100;
    vwr.FrameRate = round(intp_states/delta_t);
    open(vwr);
end

%% Robot and Dynamics setup
%-------------Trajectory time and number of states
params.support_states = time_horizon/delta_t;

%-------------Arm model
params.ndim = 7;
params.dim = 3*params.ndim;
alpha = [-1.5708, 1.5708, -1.5708, 1.5708, -1.5708, 1.5708, 0]';
a = [0.1, 0, 0, 0, 0, 0, 0]';
d = [0, 0, 0.4, 0, 0.321, 0, 0]';
theta = [0, 1.5708, 0, 0, 0, 0, 0]';
arm_origin = [-0.18053, -0.0441087, 0.958925]';
base_pose = Pose3(Rot3([0.646842  0.762624   0;
                       -0.762624  0.646842  -0;
    	               -0         0          1]), Point3(arm_origin));
arm = Arm(7, a, alpha, d, base_pose, theta);
spheres_data = [...
      0 -0.010000 0.000000 0.000000 0.180000

      2 0.015000 0.220000 -0.000000 0.110000
      2 0.035000 0.140000 -0.000000 0.080000
      2 0.035000 0.072500 -0.000000 0.080000
      2 0.000000 0.000000 -0.000000 0.105000

      4 -0.005000 0.321-0.130000 -0.000000 0.075000
      4 0.010000 0.321-0.200000 -0.025000 0.055000
      4 0.010000 0.321-0.200000 0.025000 0.055000
      4 0.015000 0.321-0.265000 -0.027500 0.050000
      4 0.015000 0.321-0.265000 0.027500 0.050000
      4 0.005000 0.321-0.320000 -0.022500 0.050000
      4 0.005000 0.321-0.320000 0.022500 0.050000

      6 0 -0.017500 0.072500 0.040000
      6 0 0.017500 0.072500 0.040000
      6 0 0 0.092500 0.040000

      6 0 0.03600 0.11 0.040000
      6 0 0.027000 0.155 0.035000
      6 0 0.00900 0.18 0.030000
      6 0 0.00950 0.205 0.020000

      6 0 -0.03600 0.11 0.040000
      6 0 -0.027000 0.155 0.035000
      6 0 -0.00900 0.18 0.030000
      6 0 -0.00950 0.205 0.020000];
nr_body = size(spheres_data, 1);
sphere_vec = BodySphereVector;
for i=1:nr_body
    sphere_vec.push_back(BodySphere(spheres_data(i,1), spheres_data(i,5), ...
        Point3(spheres_data(i,2:4)')));
end
arm = ArmModel(arm, sphere_vec);
%-------------State, Control and Dynamics
e = eye(params.ndim); z = zeros(params.ndim);
Aq = [z, e; z, z];
Bq = [z; e];
Au = [z];
Fq = [z; e];
Fu = [e];
A = [Aq, Bq; z, z, Au];
F = blkdiag(Fq, Fu);
Kwx = Qx * eye(params.ndim);
Kwu = Qu * eye(params.ndim);
W = blkdiag(Kwx, Kwu);
R = Qm*eye(params.ndim);

Phi = expm(A*delta_t);
fun = @(s) expm(A*s)*F*W*F'*expm(A*s)';
Q = integral(fun, 0, delta_t, 'ArrayValued', 1);
mu = zeros(params.dim,1);

%-------------GP interpolation
if intp_states
    [Lambda, Psi] = getLambdaAndPsi(A,F,W,delta_t,intp_states);
end

%% Initializations
rng(seed);
fix_cov = noiseModel.Isotropic.Sigma(params.ndim, Qfix);
goal_cov = noiseModel.Isotropic.Sigma(params.ndim, Qgoal);

curr_robot_pos = start;
crp_ws = arm.fk_model().forwardKinematicsPosition(curr_robot_pos);
crp_ws = crp_ws(:,end);
g_ws = arm.fk_model().forwardKinematicsPosition(goal);
g_ws = g_ws(:,end);
s_ws = crp_ws;
curr_time = 0;
curr_step = 0;

init_vel = zeros(params.ndim,1);
init_control = zeros(params.ndim,1);

% env
origin = [-1.2, -1.2, -1.2]' + arm_origin;
cell_size = 0.02;
origin_point3 = Point3(origin);
field_raw = load('sdf/industrial2.txt');
field_x = 120;
field_y = 120;
field_z = 120;
field = [];
for i = 1:field_z
    field(:,:,i) = [field_raw((i-1)*field_x+1:i*field_x, :)];
end
sdf = SignedDistanceField(origin_point3, cell_size, size(field, 1), ...
    size(field, 2), size(field, 3));
for z = 1:size(field, 3)
    sdf.initFieldData(z-1, field(:,:,z));
end

% Plot env setup
h = figure(1); clf(1);
set(h, 'Position', [-1200, 100, 1100, 1200]);
hold on; axis off;
plotSignedDistanceField3D(field, origin, cell_size);
hcp = plotRobotModel(arm, start);
plotArm(arm.fk_model(), goal, 'r', 2);
view(-69,24);
zoom(1.5);
hold off
pause(0.001);

if save_traj
    delete('traj/*');
end

%% MPC receding horizon
result = 0;
tau = 1;
past_traj = start;
Mu2 = [];
hmap = [];
for iter=1:maxiter
    disp(['****************Iter:',num2str(iter),', Goal Norm:',num2str(norm(crp_ws-g_ws))]);
    
    %-------------init optimization
    graph = NonlinearFactorGraph;
    init_values = Values;
    %goal_cov = noiseModel.Isotropic.Sigma(params.ndim, Qgoal*norm(crp_ws - g_ws)/norm(s_ws - g_ws));
    goal_cov = noiseModel.Isotropic.Sigma(params.ndim, Qgoal*norm(curr_robot_pos - goal)/norm(start - goal));
    for i = 1:params.support_states
        % t and t_1 are support states
        t = (i-2)*intp_states + i - 1;
        t_1 = (i-1)*intp_states + i;

        key_pos = symbol('x', i-1);
        key_vel = symbol('v', i-1);
        key_control = symbol('u', i-1);

        if iter==1
            init_values.insert(key_pos, start);
            init_values.insert(key_vel, init_vel);
            init_values.insert(key_control, init_control);
            if i==1 % start
                graph.add(PriorFactorVector(key_pos, start, fix_cov));
                graph.add(PriorFactorVector(key_vel, init_vel, fix_cov));
                graph.add(PriorFactorVector(key_control, init_control, fix_cov));
            elseif i==params.support_states % end
                graph.add(PriorFactorVector(key_pos, goal, goal_cov));
                graph.add(PriorFactorVector(key_vel, init_vel, goal_cov));
                graph.add(PriorFactorVector(key_control, init_control, goal_cov));
            end
        else
            % initilize from previous iteration (one step move ahead)
            if i==1 % start
                init_values.insert(key_pos, Mu2(1:params.ndim,end));
                init_values.insert(key_vel, Mu2(params.ndim+1:2*params.ndim,end));
                init_values.insert(key_control, Mu2(2*params.ndim+1:end,end));
                graph.add(PriorFactorVector(key_pos, Mu2(1:params.ndim,end), fix_cov));
                graph.add(PriorFactorVector(key_vel, Mu2(params.ndim+1:2*params.ndim,end), fix_cov));
                graph.add(PriorFactorVector(key_control, Mu2(2*params.ndim+1:end,end), fix_cov));
            elseif i==params.support_states % end
                init_values.insert(key_pos, result.atVector(symbol('x', i-1)));
                init_values.insert(key_vel, result.atVector(symbol('v', i-1)));
                init_values.insert(key_control, result.atVector(symbol('u', i-1)));
                graph.add(PriorFactorVector(key_pos, goal, goal_cov));
                graph.add(PriorFactorVector(key_vel, init_vel, goal_cov));
                graph.add(PriorFactorVector(key_control, init_control, goal_cov));
            else
                init_values.insert(key_pos, result.atVector(symbol('x', i)));
                init_values.insert(key_vel, result.atVector(symbol('v', i)));
                init_values.insert(key_control, result.atVector(symbol('u', i)));
                graph.add(PriorFactorVector(key_pos, goal, goal_cov));
                graph.add(PriorFactorVector(key_vel, init_vel, goal_cov));
                graph.add(PriorFactorVector(key_control, init_control, goal_cov));
            end 
        end

        if i > 1
            % GP factor
            key_pos1 = symbol('x', i-2);
            key_pos2 = symbol('x', i-1);
            key_vel1 = symbol('v', i-2);
            key_vel2 = symbol('v', i-1);
            key_control1 = symbol('u', i-2);
            key_control2 = symbol('u', i-1);
            graph.add(GPCPriorLinearFactor(key_pos1, key_vel1, key_control1, ...
                key_pos2, key_vel2, key_control2, mu, Q, Phi));

            % Obstacle cost factor
            graph.add(ObstacleSDFFactorArm(...
                key_pos, arm, sdf, Qobs, epsilon_dist));

            % GP cost factor
            for j = 1:intp_states
                tau = tau + 1;
                graph.add(GPCIntpObstacleFactorArm( ...
                        key_pos1, key_vel1, key_control1, key_pos2, key_vel2, key_control2, ...
                        arm, sdf, Qobs, epsilon_dist, Lambda(:,:,j), Psi(:,:,j)));
            end
        end
        
        tau = tau + 1;
    end
    %-------------Optimization
    use_LM = true;
    use_trustregion_opt = false;
    if use_LM
        parameters = LevenbergMarquardtParams;
        parameters.setVerbosity('TERMINATION');
        parameters.setlambdaInitial(1000.0);
        optimizer = LevenbergMarquardtOptimizer(graph, init_values, parameters);
    elseif use_trustregion_opt
        parameters = DoglegParams;
        parameters.setVerbosity('TERMINATION');
        optimizer = DoglegOptimizer(graph, init_values, parameters);
    else
        parameters = GaussNewtonParams;
        parameters.setVerbosity('TERMINATION');
        optimizer = GaussNewtonOptimizer(graph, init_values, parameters);
    end
    tic
    optimizer.optimize();
    toc
    result = optimizer.values();

    %-------simulate
    [Mu, K] = getPosterior(graph,result,params);
    delta_t_rp = delta_t/(intp_states+1);
    Phi_rp = expm(A*delta_t_rp);
    Int_rp = integral(fun, 0, delta_t_rp, 'ArrayValued', 1);
    Mu_0 = Mu(1:params.dim);
    K_0 = K(1:params.dim,1:params.dim);
    gp = GPInterpolator(1, delta_t, params.ndim, intp_states, A, F, W);
    gp.interpolate(Mu(1:2*params.dim));
    planner = Replanner(params.ndim, gp, R, Mu(1:2*params.dim), K(1:2*params.dim,1:2*params.dim));
    %planner = gp;
    Mu2 = simulate(1, intp_states, params.ndim, Phi_rp, Int_rp, R, planner, Mu_0, K_0, true);
    %Mu2 = gp.V_';
    
    %-------------Plot env and trajectory
    figure(1);
    hold on;
    delete(hmap);
    Mu = reshape(Mu,[params.dim params.support_states]);
    pos_all = [];
    for i=2:params.support_states
        hmap(i,:) = plotArm(arm.fk_model(), Mu(1:params.ndim,i), 'b', 2);
        if save_traj
            pos = arm.fk_model().forwardKinematicsPosition(Mu(1:params.ndim,i));
            pos_all = [pos_all pos(:,end)];
        end
    end
    if save_traj
        dlmwrite(['traj/', num2str(iter), '.txt'],pos_all','delimiter','\t','precision','%.3f');
    end
    hold off;
    for i=2:size(Mu2,2)
        curr_robot_pos = Mu2(1:params.ndim,i);
        figure(1);
        hold on;
        % delete(hcp); hcp = plotArm(arm.fk_model(), curr_robot_pos, 'b', 2);
        delete(hcp); hcp = plotRobotModel(arm, curr_robot_pos);
        hold off;
        if output_video
            currFrame = getframe(h);
            writeVideo(vwr, currFrame);
        end
        past_traj = [past_traj curr_robot_pos];
        pause(0.001);
    end
    
    crp_ws = arm.fk_model().forwardKinematicsPosition(curr_robot_pos);
    crp_ws = crp_ws(:,end);

    if (norm(crp_ws - g_ws) < 1e-2)
        disp('DONE');
        break;
    end
end

%%
if save_traj
    dlmwrite(['traj/mpc.txt'],past_traj','delimiter','\t','precision','%.3f');
end
if output_video
    close(vwr);
end