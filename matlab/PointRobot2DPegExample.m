% @brief    Point Robot 2D peg board navigation example
% @author   Eric Huang
% @date     August 10, 2016

close all
clear

%% Load libraries
import gtsam.*
import gpmp2.*
import graph.*

%% small dataset
dataset = generatePegDataset('');
rows = dataset.rows;
cols = dataset.cols;
cell_size = dataset.cell_size;
origin_point2 = Point2(dataset.origin_x, dataset.origin_y);

% signed distance field
field = signedDistanceField2D(dataset.map, cell_size);
sdf = PlanarSDF(origin_point2, cell_size, field);

% plot sdf
figure(1)
plotSignedDistanceField2D(field, dataset.origin_x, dataset.origin_y, dataset.cell_size);
title('Signed Distance Field')

%% settings
total_time_sec = 5.0;
total_time_step = 10;
total_check_step = 50;
delta_t = total_time_sec / total_time_step;
check_inter = total_check_step / total_time_step - 1;

% use GP interpolation
use_GP_inter = true;

% point robot model
pR = PointRobot(2,1);
spheres_data = [0  0.0  0.0  0.0  0.5];
nr_body = size(spheres_data, 1);
sphere_vec = BodySphereVector;
sphere_vec.push_back(BodySphere(spheres_data(1,1), spheres_data(1,5), ...
        Point3(spheres_data(1,2:4)')));
pR_model = PointRobotModel(pR, sphere_vec);

% GP
Qc = eye(2);
Qc_model = noiseModel.Gaussian.Covariance(Qc); 

% obstacle cost settings
cost_sigma = 0.001;
epsilon_dist = 2;

% prior to start/goal
pose_fix = noiseModel.Isotropic.Sigma(2, 0.0001);
vel_fix = noiseModel.Isotropic.Sigma(2, 0.0001);

% start and end conf
start_conf = [-15, 5]';
start_vel = [0, 0]';
end_conf = [15, 5]';
end_vel = [0, 0]';
avg_vel = (end_conf / total_time_step) / delta_t;

% plot param
pause_time = total_time_sec / total_time_step;

% % plot start / end configuration
figure(1), hold on
plotEvidenceMap2D(dataset.map, dataset.origin_x, dataset.origin_y, cell_size);
plotPointRobot2D(pR_model, start_conf);
plotPointRobot2D(pR_model, end_conf);
title('Layout')
hold off

%% init graph
n_pts = 5;
G = generateDelaunayGraph(start_conf, end_conf, avg_vel, dataset, n_pts);
figure(1), hold on
plotGraph2D(G);

%% init optimization
graph = NonlinearFactorGraph;
init_values = Values;

% Get graph variables.
num_nodes = size(G.Nodes,1);
num_edges = size(G.Edges,1);
V = G.Nodes;
E = G.Edges.EndNodes;

% add nodes.
for i = 1:num_nodes
    % symbols
    key_pos = symbol('x', i);
    key_vel = symbol('v', i);
    % values
    pose = V.Pos(i,:)';
    vel  = V.Vel(i,:)';
    init_values.insert(key_pos, pose);
    init_values.insert(key_vel, vel);
    % priors
    if i==1
        graph.add(PriorFactorVector(key_pos, pose, pose_fix));
        graph.add(PriorFactorVector(key_vel, vel, vel_fix));
    elseif i==num_nodes
        graph.add(PriorFactorVector(key_pos, pose, pose_fix));
        graph.add(PriorFactorVector(key_vel, vel, vel_fix));
    else
        % cost factor
        graph.add(ObstaclePlanarSDFFactorPointRobot(...
            key_pos, pR_model, sdf, cost_sigma, epsilon_dist));
    end
end

% add factors
for i = 1:num_edges
    % indexes
    ii = findnode(G, E(i,:));
    % symbols
    key_pos1 = symbol('x', ii(1));
    key_vel1 = symbol('v', ii(1));
    key_pos2 = symbol('x', ii(2));
    key_vel2 = symbol('v', ii(2));
    % factors
    graph.add(GaussianProcessPriorLinear(key_pos1, key_vel1, ...
        key_pos2, key_vel2, delta_t, Qc_model));
    % GP cost factor
    if use_GP_inter && check_inter > 0
        for j = 1:check_inter
            tau = j * (total_time_sec / total_check_step);
            graph.add(ObstaclePlanarSDFFactorGPPointRobot( ...
                key_pos1, key_vel1, key_pos2, key_vel2, ...
                pR_model, sdf, cost_sigma, epsilon_dist, ...
                Qc_model, delta_t, tau));
        end
    end
end

%% plot initial graph
E = G.Edges.EndNodes;
num_edges = size(E,1);

figure(1), hold on
title('Initial Values')
plotEvidenceMap2D(dataset.map, dataset.origin_x, dataset.origin_y, cell_size);
plotGraph2D(G)
for i = 1:num_edges
    % indexes
    ii = findnode(G, E(i,:));
    x1 = init_values.atVector(symbol('x', ii(1)))
    x2 = init_values.atVector(symbol('x', ii(2)))
    plot([x1(1) x2(1)], [x1(2) x2(2)], 'b');
end
hold off

%% optimize!
use_trustregion_opt = false;

if use_trustregion_opt
    parameters = DoglegParams;
    parameters.setVerbosity('ERROR');
    optimizer = DoglegOptimizer(graph, init_values, parameters);
else
    parameters = GaussNewtonParams;
    parameters.setVerbosity('ERROR');
    optimizer = GaussNewtonOptimizer(graph, init_values, parameters);
end
%%
tic
optimizer.iterate();
toc
result = optimizer.values();
% result.print('Final results')

% plot final graph
E = G.Edges.EndNodes;
num_edges = size(E,1);

figure(1), hold on
title('Optimized Values')
plotEvidenceMap2D(dataset.map, dataset.origin_x, dataset.origin_y, cell_size);
for i = 1:num_edges
    % indexes
    ii = findnode(G, E(i,:));
    x1 = result.atVector(symbol('x', ii(1)));
    x2 = result.atVector(symbol('x', ii(2)));
    plot([x1(1) x2(1)], [x1(2) x2(2)], 'b');
end
hold off
