%% Generate batch test specification.

import gpmp2.*
import graph.*
import gtsam.*

% Batch test filepath.
batch_test_loc = '~/icra_2017/data/000e_maze/batch.txt';

% Make batch test folder.
[batch_dir,~, ~] = fileparts(batch_test_loc);
if ~exist(batch_dir, 'dir'), mkdir(batch_dir); end

% Batch test summary.
batch_test_md = [batch_dir '/' 'readme.md'];
btm_id = fopen(batch_test_md, 'w');

% Dataset function handles.
dataset_funcs = cell(0);
% dataset_funcs{end+1} = '@() graph.generateRandomPegsDataset(0.1)';
% dataset_funcs{end+1} = '@() graph.generateRandomPegsDataset(0.2)';
% dataset_funcs{end+1} = '@() graph.generateRandomPegsDataset(0.3)';
% dataset_funcs{end+1} = '@() graph.generateRandomPegsDataset(0.4)';
% dataset_funcs{end+1} = '@() graph.generateRandomPegsDataset(0.5)';
dataset_funcs{end+1} = '@() graph.generateUniformMazeDataset(3, 3)';
dataset_funcs{end+1} = '@() graph.generateUniformMazeDataset(4, 4)';
% dataset_funcs{end+1} = '@() graph.generateUniformMazeDataset(5, 5)';
dataset_funcs = dataset_funcs';
fprintf(btm_id, '%s\n', dataset_funcs{:});

% Dataset test count.
genpd = 200;

% Planner function handles.
planner_funcs = cell(0);
planner_funcs{end+1} = '@(gp) graph.generateRandomBranchedGraph(52,5,gp)';
planner_funcs{end+1} = '@(gp) graph.generateRandomBranchedGraph(30,5,gp)';
planner_funcs{end+1} = '@(gp) graph.generateRandomBranchedGraph(15,5,gp)';
planner_funcs{end+1} = '@(gp) graph.generateRandomBranchedGraph(5,5,gp)';
planner_funcs{end+1} = '@(gp) generateBranchedGraph(5,gp)';
planner_funcs{end+1} = '@(gp) graph.generateStraightLineGraph(gp)';
fprintf(btm_id, '%s\n', planner_funcs{:});

% Factor graph converter function handles.
convert_funcs = cell(0);
convert_funcs{end+1} = ...
    '@(G,gp,r,d) graph.convertToFactorGraph(G,gp,r,d)';
% convert_funcs{end+1} = ...
%     '@(G,gp,r,d) graph.convertToFactorGraph(G,gp,r,d,''CovGPPriorLinear'',25)';
fprintf(btm_id, '%s\n', convert_funcs{:});

% Refine MAP trajectory.
refine_funcs = cell(0);
refine_funcs{end+1} = '@() 1';
refine_funcs{end+1} = '@() 0';

% Generate batch test spec.
num_tests = 0;
fid = fopen(batch_test_loc, 'w');
fprintf(fid, 'dataset; planner; convert; d_i; p_i; c_i\n');
for d_i = 1:length(dataset_funcs)
    for p_i = 1:length(planner_funcs)
        for c_i = 1:length(convert_funcs)
            for g_i = 1:genpd
                d = dataset_funcs{d_i};
                p = planner_funcs{p_i};
                c = convert_funcs{c_i};
                fprintf(fid, '%s; %s; %s; %d; %d; %d\n', d, p, c, d_i, p_i, c_i);
                num_tests = num_tests + 1;
            end
        end
    end
end
fclose(fid);
fclose(btm_id);