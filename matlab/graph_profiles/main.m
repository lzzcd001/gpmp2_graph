%% Run script

% Load batch test table.
batch = readtable(batch_test_loc, 'Delimiter', ';');

% Get batch test folder.
[batch_dir,~, ~] = fileparts(batch_test_loc);

% Add outputs.
num_tests = size(batch, 1);
batch.elapsedTime = zeros([num_tests,1]);
batch.collisionFree = zeros([num_tests,1]);
batch.collisionCheckTime = zeros([num_tests,1]);
batch.G = cell([num_tests, 1]);

% Run.
tic
h = figure(1);
h.Visible = 'off';
for i = 1:num_tests

    % Get variations.
    dataset_func = str2func(batch.dataset{i});
    planner_func = str2func(batch.planner{i});
    convert_func = str2func(batch.convert{i});

    % Run script.
    run('pointRobot2DGraphPlanningScript');

    % Outputs.
    batch.elapsedTime(i) = elapsedTime;
    batch.collisionFree(i) = collisionFree;

    % Save results to file.
%     results_dir = [batch_dir '/exp_' num2str(batch.d_i(i)) num2str(batch.p_i(i))];
%     if ~exist(results_dir, 'dir'), mkdir(results_dir); end
%     clf(h);
%     h.Visible = 'off';
%     results_fig = [results_dir '/' 'fig' num2str(i) '.jpeg'];
%     title('Optimized Values')
%     hold on
%     plotEvidenceMap2D(dataset.map, dataset.origin_x, dataset.origin_y, cell_size);
%     plotPointRobot2D(robot.pR_model, gp.x0);
%     plotPointRobot2D(robot.pR_model, gp.xN);
%     plotGraph2D(G, gp.Qc_model);
%     print(results_fig, '-djpeg');

    % Clear.
    % close all
    clearvars -except batch i num_tests batch_test_loc batch_dir h
end
toc

%% Plot results
d_max = max(batch.d_i);
p_max = max(batch.p_i);
c_max = max(batch.c_i);

collisionFree = zeros([d_max, p_max]);
elapsedTime   = zeros([d_max, p_max]);
for d_i = 1:d_max
    for p_i = 1:p_max
        ii = and(batch.d_i == d_i, batch.p_i == p_i);
        collisionFree(d_i, p_i) = sum(batch.collisionFree(ii)) ./ sum(ii);
        elapsedTime(d_i, p_i) = mean(batch.elapsedTime(ii));
    end
end
collisionFree
elapsedTime

% Write to file.
batch_results = [batch_dir '/' 'results.md'];
fid = fopen(batch_results, 'w');
fprintf(fid, 'collisionFree\n');
appendToFile(fid, collisionFree, '%5.4f ');
fprintf(fid, 'elapsedTime\n');
appendToFile(fid, elapsedTime, '%5.4f ');
fclose(fid);
