function [ success ] = appendToFile( fid, A, fmt )
%APPENDTOFILE 
%   

%% 
for i = 1:size(A, 1)
    fprintf(fid, fmt, A(i,:));
    fprintf(fid, '\n');
end
success = 1;

