% @brief    Point robot two peg example.
% @author   Eric Huang
% @date     August 10, 2016

close all
clear 

%% Load libraries
import gtsam.*
import gpmp2.*
import graph.*

%% small dataset
[dataset, x0, xN] = generateUniformMazeDataset(3,3);
rows = dataset.rows;
cols = dataset.cols;
cell_size = dataset.cell_size;
origin_point2 = Point2(dataset.origin_x, dataset.origin_y);

% signed distance field
field = signedDistanceField2D(dataset.map, cell_size);
sdf = PlanarSDF(origin_point2, cell_size, field);

% plot sdf
% figure(1)
% plotSignedDistanceField2D(field, dataset.origin_x, dataset.origin_y, dataset.cell_size);
% title('Signed Distance Field')

% dataset
dataset.field = field;
dataset.sdf = sdf;

%% settings

% time steps
total_time_sec = 5.0;
total_time_step = 10;
total_check_step = 50;
delta_t = total_time_sec / total_time_step;
check_inter = total_check_step / total_time_step - 1;

% use GP interpolation
use_GP_inter = true;

% point robot model
pR = PointRobot(2,1);
spheres_data = [0  0.0  0.0  0.0  0.5];
nr_body = size(spheres_data, 1);
sphere_vec = BodySphereVector;
sphere_vec.push_back(BodySphere(spheres_data(1,1), spheres_data(1,5), ...
        Point3(spheres_data(1,2:4)')));
pR_model = PointRobotModel(pR, sphere_vec);

% GP
Qc = 6 * eye(2);
Qc_model = noiseModel.Gaussian.Covariance(Qc);

% obstacle cost settings
cost_sigma = 0.3;
epsilon_dist = 2;

% prior to start/goal
pose_fix = noiseModel.Isotropic.Sigma(2, 0.0001);
vel_fix = noiseModel.Isotropic.Sigma(2, 0.0001);

% start and end conf
% start_conf = [-18, -8]';
start_conf = x0;
start_vel = [0, 0]';
% end_conf = [18, 15]';
end_conf = xN;
end_vel = [0, 0]';
avg_vel = (end_conf / total_time_step) / delta_t;

% gp struct
gp = struct;
gp.t = linspace(0, total_time_sec, total_time_step);
gp.Qc_model = Qc_model;
gp.pose_fix = pose_fix;
gp.vel_fix = vel_fix;
gp.x0 = start_conf;
gp.xN = end_conf;
gp.vel = avg_vel;
gp.use_GP_inter = use_GP_inter;
gp.total_time_sec = total_time_sec;
gp.total_time_step = total_time_step;
gp.total_check_step = total_check_step;
gp.delta_t = total_time_sec / total_time_step;
gp.check_inter = total_check_step / total_time_step - 1;
gp.cost_sigma = cost_sigma;
gp.epsilon_dist = epsilon_dist;

% robot struct
robot = struct;
robot.pR_model = pR_model;

% plot param
pause_time = total_time_sec / total_time_step;

% plot start / end configuration
% figure(1), hold on
% plotEvidenceMap2D(dataset.map, dataset.origin_x, dataset.origin_y, cell_size);
% plotPointRobot2D(pR_model, start_conf);
% plotPointRobot2D(pR_model, end_conf);
% title('Layout')
% hold off

%% Test graph.
G = generateStraightLineGraph(gp);
G = generateBranchedGraph(6, gp);
G = generateRandomBranchedGraph(10, 5, gp);
% figure(1), hold on
% plotGraph2D(G, Qc_model);



