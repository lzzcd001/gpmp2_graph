%% 
import gtsam.*
import gpmp2.*
import graph.*

pR = PointRobot(2,1);
spheres_data = [0  0.0  0.0  0.0  0.5];
nr_body = size(spheres_data, 1);
sphere_vec = BodySphereVector;
sphere_vec.push_back(BodySphere(spheres_data(1,1), spheres_data(1,5), ...
        Point3(spheres_data(1,2:4)')));
pR_model = PointRobotModel(pR, sphere_vec);

clc
%%
dataset = generateRandomPegsDataset(0.3);
plotEvidenceMap2D(dataset.map, dataset.origin_x, dataset.origin_y, cell_size);

%% 
[dataset, x0, xN] = generateUniformMazeDataset(6,6);
cell_size = dataset.cell_size;
origin_point2 = Point2(dataset.origin_x, dataset.origin_y);

% signed distance field
field = signedDistanceField2D(dataset.map, cell_size);
sdf = PlanarSDF(origin_point2, cell_size, field);

% plot sdf
epsilon_dist = 1.0;
figure(1)
title('Signed Distance Field')
subplot(211)
hold on
plotSignedDistanceField2D(field, dataset.origin_x, dataset.origin_y, dataset.cell_size, epsilon_dist);
plotPointRobot2D(pR_model, x0);
plotPointRobot2D(pR_model, xN);
subplot(212)
hold on
plotEvidenceMap2D(dataset.map, dataset.origin_x, dataset.origin_y, cell_size);
plotPointRobot2D(pR_model, x0);
plotPointRobot2D(pR_model, xN);