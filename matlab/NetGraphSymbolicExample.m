%% Generate symbolic inverse of net graph covariance matrices.

N = 5;
Q = sym('P0');
for i = 1:N
    Q = [Q sym(['Q' num2str(i)], 'real')];
end
Q = diag(Q);

F1_inv = sym(eye(N+1));
for i = 0:N-1
    F1_inv(i+2,i+1) = -sym(['P1_' num2str(i+1) num2str(i)], 'real');
end
F1_inv;

F2_inv = sym(eye(N+1));
for i = 0:N-1
    F2_inv(i+2,i+1) = -sym(['P2_' num2str(i+1) num2str(i)], 'real');
end
F2_inv;

P1_inv = F1_inv' * inv(Q) * F1_inv;
P2_inv = F2_inv' * inv(Q) * F2_inv;

% P1_inv(4,3) = sym('x','real')
% P1_inv(3,4) = sym('x','real')

P1 = inv(P1_inv)

%% 
clc
PP_inv = [P1_inv sym(zeros(N+1)); sym(zeros(N+1)) P2_inv]
PP = inv(PP_inv)

x = (sqrt(2)*sym('P0', 'real'))^(-1);
x = P1_inv(2,1)
x = sym('x', 'real');
PP_inv(N+2,1) = x;
PP_inv(1,N+2) = x

assert(isequal(PP_inv, PP_inv'))

tmp = PP_inv(:,1);
PP_inv(:,1) = PP_inv(:,3);
PP_inv(:,3) = tmp

tmp = PP_inv(1,:);
PP_inv(1,:) = PP_inv(3,:);
PP_inv(3,:) = tmp

PP = inv(PP_inv)

% PP = simplify(inv(PP_inv))

% PP(N+2,N+2)