function [ G ] = generateGraph( A, pos, vel, t )
%GENERATEGRAPH Generate graph from matrix.
%   

%% Create graph.
N = length(A);
G = graph(A);

% Create nodes.
names = cell([N,1]);
for i = 1:N
    names{i} = ['x' num2str(i, '%d')];
end
G.Nodes.Name = names;
G.Nodes.Pos = pos;
G.Nodes.Vel = vel;
G.Nodes.Time = t;

% Store edge indexes for efficiency.
num_edges = size(G.Edges,1);
E = G.Edges.EndNodes;
Pairs = zeros([num_edges, 2]);
for i = 1:num_edges
    Pairs(i,:) = findnode(G, E(i,:));
end
G.Edges.Pairs = Pairs;

end

