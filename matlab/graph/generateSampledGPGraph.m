function [ output_args ] = generateTriconnectedGraph( input_args )
%GENERATETRICONNECTEDGRAPH Summary of this function goes here
%   Detailed explanation goes here

%% FIXME
x1 = [start_conf ; avg_vel];
xN = [end_conf ; avg_vel];
t = delta_t * (0 : total_time_step);
Qc_init = 10 * eye(4);

n_branches = 10;
n_dim = length(x1);
X = zeros([n_branches, n_dim * (total_time_step+1)]);
for i = 1:n_branches
    x = gp_cond1N(t, x1, xN, Qc_init);
    X(i,n_dim+1:end-n_dim) = x;
    X(i,1:n_dim) = x1;
    X(i,end-n_dim+1:end) = xN;
end

% init graph structure

clc

% params
n_t = length(0:total_time_step);
n_b = n_branches;
n_e = 3;
n_V = (n_t-2)*n_b + 2;
% nodes
V = repmat(struct('t',0,'b',0,'d',[],'i',0), [n_V, 1]);
% start
node = struct;
node.b = 1;
node.t = 1;
node.d = [];
node.i = 1;
V(1) = node;
% end
node.b = 1;
node.t = n_t;
node.d = [];
node.i = n_V;
V(end) = node;
% start children
for i = 1:n_b
    V(1).d = 2:2+n_b-1;
end
% end parents
for i = 1:n_b
    V(n_V-n_b+i-1).t = n_t-1;
    V(n_V-n_b+i-1).b = i;
    V(n_V-n_b+i-1).d = n_V;
    V(n_V-n_b+i-1).i = n_V-n_b+i-1;
end
% branches
for t = 1:n_t-3
    for b = 1:n_b
        i = 1 + (t-1)*n_b + b;
        d = b + (-(n_e-1)/2:(n_e-1)/2);
        d = d(and(1 <= d, d <= n_b));
        d = d - b;
        V(i).t = t;
        V(i).b = b;
        V(i).d = i + n_b + d;
        V(i).i = i;
    end
end
for i = 1:n_V
    V(i)
end
% edges
E = [];
for i = 1:n_V
    for j = 1:length(V(i).d)
        e = struct;
        e.l = V(i);
        e.r = V(V(i).d(j));
        E = [E; e]; %#ok<AGROW>
    end
end
n_E = length(E);

% plot
hold on
plotEvidenceMap2D(dataset.map, dataset.origin_x, dataset.origin_y, cell_size);
for i = 1:n_E
    l = E(i).l;
    r = E(i).r;
    t_l = (l.t-1)*n_dim + (1:n_dim);
    t_r = (r.t-1)*n_dim + (1:n_dim);
    x1 = X(l.b, t_l);
    x2 = X(r.b, t_r);
    plot([x1(1) x2(1)], [x1(2) x2(2)]);
end
hold off

end

