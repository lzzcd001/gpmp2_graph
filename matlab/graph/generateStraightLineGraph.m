function [ G ] = generateStraightLineGraph( gp )
%GENERATESTRAIGHTLINEGRAPH Generate straight line graph.
%   

%% Generate straight line initialization.
import graph.*

start_conf = gp.x0;
end_conf = gp.xN;
n_pts = length(gp.t)-2;
t_max = gp.t(end);

A = zeros(n_pts+2);
for i = 1:n_pts+1
    A(i,i+1) = 1;
    A(i+1,i) = 1;
end
pos = zeros([n_pts+2, 2]);
pos(:,1) = linspace(start_conf(1), end_conf(1), n_pts + 2);
pos(:,2) = linspace(start_conf(2), end_conf(2), n_pts + 2);
avg_vel = (pos(end,:) - pos(1,:))./t_max;
vel = repmat(avg_vel, [n_pts+2, 1]);

G = generateGraph(A, pos, vel, gp.t');

end

