function [ ] = plotGraph2D( G, Qc_model, varargin )
%PLOTGRAPH2D Plot a 2D graph.
%   

import gpmp2.*

p = inputParser;
addParameter(p, 'color', 'b');
parse(p, varargin{:});

%% Plot 2D graph.
num_interp = 10;
num_edges = size(G.Edges, 1);
E = G.Edges.EndNodes;
x = G.Nodes.Pos;
v = G.Nodes.Vel;
t = G.Nodes.Time;
hold on
for i = 1:num_edges
    % Get edge node positions and velocities.
    k = findnode(G, E(i,:));
    x1 = x(k(1),:)';
    x2 = x(k(2),:)';
    v1 = v(k(1),:)';
    v2 = v(k(2),:)';
    dt = t(k(2)) - t(k(1));
    % GP interpolation.
    x_tau = zeros([2, num_interp+1]);
    for j = 0:num_interp
        tau = j / num_interp * dt;
        gp = GaussianProcessInterpolatorLinear(Qc_model, dt, tau);
        x_tau(:,j+1) = gp.interpolatePose(x1, v1, x2, v2);
    end
    plot(x_tau(1,:), x_tau(2,:),[p.Results.color '-']);
    plot([x1(1) x2(1)], [x1(2) x2(2)], [p.Results.color 'x']);
    % Plot interpolated values.
%     xx = x(k,1);
%     yy = x(k,2);
%     plot(xx,yy, 'b-x')
end

