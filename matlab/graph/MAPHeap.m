classdef MAPHeap < handle
    %MAPLIST 
    %   
    
    properties
        a = graph.MAPNode;
        n = 0;
    end
    
    methods
        function obj = MAPHeap(num_el)
            obj.a(num_el) = graph.MAPNode;
        end
        
        function out = empty(obj)
            out = obj.n == 0;
        end
        
        function push(obj, u)
            % Sift up
            if u.h
                % If pushing old node on heap, assume decrease-key op.
                u_i = u.h;
            else
                u_i = obj.n + 1;
            end
            p_i = floor(u_i/2);
            obj.a(u_i) = u;
            while u_i ~= 1
                p = obj.a(p_i);
                if p.c <= u.c
                    break
                end
                obj.a(u_i) = p;
                obj.a(p_i) = u;
                u_i = p_i;
                p_i = floor(u_i/2);
            end
            if ~u.h
                obj.n = obj.n + 1;
            end
            % Mark node as on heap.
            u.h = u_i;
        end

        function r = pop(obj)
            % Get root.
            r = obj.a(1);
            % Mark node as off heap.
            r.h = 0;
            % Sift down.
            obj.a(1) = obj.a(obj.n);
            obj.n = obj.n - 1;
            u_i = 1;
            u = obj.a(u_i);
            while u_i ~= obj.n
                s_i = u_i;
                l_i = 2*u_i;
                if l_i <= obj.n && obj.a(l_i).c < obj.a(s_i).c
                    s_i = l_i;
                end
                r_i = 2*u_i+1;
                if r_i <= obj.n && obj.a(r_i).c < obj.a(s_i).c
                    s_i = r_i;
                end
                if s_i == u_i
                    break
                end
                obj.a(u_i) = obj.a(s_i);
                obj.a(s_i) = u;
                u_i = s_i;
            end
        end
    end
    
end

