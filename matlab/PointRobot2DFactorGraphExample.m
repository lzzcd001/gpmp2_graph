% @brief    Point Robot 2D example, building factor graph in matlab
% @author   Mustafa Mukadam
% @date     July 20, 2016

close all
clear

%% Load libraries
import gtsam.*
import gpmp2.*

%% small dataset
dataset = generate2Ddataset('MultiObstacleDataset');
rows = dataset.rows;
cols = dataset.cols;
cell_size = dataset.cell_size;
origin_point2 = Point2(dataset.origin_x, dataset.origin_y);

% signed distance field
field = signedDistanceField2D(dataset.map, cell_size);
sdf = PlanarSDF(origin_point2, cell_size, field);

% % plot sdf
% figure(2)
% plotSignedDistanceField2D(field, dataset.origin_x, dataset.origin_y, dataset.cell_size);
% title('Signed Distance Field')


%% settings
total_time_sec = 5.0;
total_time_step = 5;
total_check_step = 50;
delta_t = total_time_sec / total_time_step;
check_inter = total_check_step / total_time_step - 1;

% use GP interpolation
use_GP_inter = true;

% point robot model
pR = PointRobot(2,1);
spheres_data = [0  0.0  0.0  0.0  1.5];
nr_body = size(spheres_data, 1);
sphere_vec = BodySphereVector;
sphere_vec.push_back(BodySphere(spheres_data(1,1), spheres_data(1,5), ...
        Point3(spheres_data(1,2:4)')));
pR_model = PointRobotModel(pR, sphere_vec);

% GP
Qc = eye(2);
Qc_model = noiseModel.Gaussian.Covariance(Qc); 

% obstacle cost settings
cost_sigma = 1.0; % 0.3
epsilon_dist = 2;

% prior to start/goal
pose_fix = noiseModel.Isotropic.Sigma(2, 0.0001);
vel_fix = noiseModel.Isotropic.Sigma(2, 0.0001);

% start and end conf
start_conf = [-15, -8]';
start_vel = [0, 0]';
end_conf = [17, 14]';
end_vel = [0, 0]';
avg_vel = (end_conf / total_time_step) / delta_t;

% plot param
pause_time = total_time_sec / total_time_step;

% % plot start / end configuration
figure(1), hold on
plotEvidenceMap2D(dataset.map, dataset.origin_x, dataset.origin_y, cell_size);
plotPointRobot2D(pR_model, start_conf);
plotPointRobot2D(pR_model, end_conf);
title('Layout')
hold off

%% init samples

x1 = [start_conf ; avg_vel];
xN = [end_conf ; avg_vel];
t = delta_t * (0 : total_time_step);
Qc_init = 100 * eye(4);

n_branches = 10;
n_dim = length(x1);
X = zeros([n_branches, n_dim * (total_time_step+1)]);
for i = 1:n_branches
    x = gp_cond1N(t, x1, xN, Qc_init);
    X(i,n_dim+1:end-n_dim) = x;
    X(i,1:n_dim) = x1;
    X(i,end-n_dim+1:end) = xN;
end

% init graph structure

clc

% params
n_t = length(0:total_time_step);
n_b = n_branches;
n_e = 3;
n_V = (n_t-2)*n_b + 2;
% nodes
V = repmat(struct('t',0,'b',0,'d',[],'i',0), [n_V, 1]);
% start
node = struct;
node.b = 1;
node.t = 1;
node.d = [];
node.i = 1;
V(1) = node;
% end
node.b = 1;
node.t = n_t;
node.d = [];
node.i = n_V;
V(end) = node;
% start children
for i = 1:n_b
    V(1).d = 2:2+n_b-1;
end
% end parents
for i = 1:n_b
    V(n_V-n_b+i-1).t = n_t-1;
    V(n_V-n_b+i-1).b = i;
    V(n_V-n_b+i-1).d = n_V;
    V(n_V-n_b+i-1).i = n_V-n_b+i-1;
end
% branches
for t = 1:n_t-3
    for b = 1:n_b
        i = 1 + (t-1)*n_b + b;
        d = b + (-(n_e-1)/2:(n_e-1)/2);
        d = d(and(1 <= d, d <= n_b));
        d = d - b;
        V(i).t = t;
        V(i).b = b;
        V(i).d = i + n_b + d;
        V(i).i = i;
    end
end
for i = 1:n_V
    V(i)
end
% edges
E = [];
for i = 1:n_V
    for j = 1:length(V(i).d)
        e = struct;
        e.l = V(i);
        e.r = V(V(i).d(j));
        E = [E; e]; %#ok<AGROW>
    end
end
n_E = length(E);

% plot
hold on
plotEvidenceMap2D(dataset.map, dataset.origin_x, dataset.origin_y, cell_size);
for i = 1:n_E
    l = E(i).l;
    r = E(i).r;
    t_l = (l.t-1)*n_dim + (1:n_dim);
    t_r = (r.t-1)*n_dim + (1:n_dim);
    x1 = X(l.b, t_l);
    x2 = X(r.b, t_r);
    plot([x1(1) x2(1)], [x1(2) x2(2)]);
end
hold off

%% init optimization
graph = NonlinearFactorGraph;
init_values = Values;

% inital values
n_states = n_dim/2;
for i = 1:n_V
    % symbols
    key_pos = symbol('x', V(i).i);
    key_vel = symbol('v', V(i).i);
    % values
    x = X(V(i).b, (V(i).t-1)*n_dim + (1:n_dim))';
    pose = x(1:n_states);
    vel  = x(n_states+1:end);
    init_values.insert(key_pos, pose);
    init_values.insert(key_vel, vel);
    % priors
    if i==1
        graph.add(PriorFactorVector(key_pos, start_conf, pose_fix));
        graph.add(PriorFactorVector(key_vel, start_vel, vel_fix));
    elseif i==n_V
        graph.add(PriorFactorVector(key_pos, end_conf, pose_fix));
        graph.add(PriorFactorVector(key_vel, end_vel, vel_fix));
    else
        % cost factor
        graph.add(ObstaclePlanarSDFFactorPointRobot(...
            key_pos, pR_model, sdf, cost_sigma, epsilon_dist));
    end
end
% factors
for i = 1:n_E
    l = E(i).l;
    r = E(i).r;
    key_pos1 = symbol('x', l.i);
    key_pos2 = symbol('x', r.i);
    key_vel1 = symbol('v', l.i);
    key_vel2 = symbol('v', r.i);
    graph.add(GaussianProcessPriorLinear(key_pos1, key_vel1, ...
        key_pos2, key_vel2, delta_t, Qc_model));
    
    % GP cost factor
    if use_GP_inter & check_inter > 0
        for j = 1:check_inter
            tau = j * (total_time_sec / total_check_step);
            graph.add(ObstaclePlanarSDFFactorGPPointRobot( ...
                key_pos1, key_vel1, key_pos2, key_vel2, ...
                pR_model, sdf, cost_sigma, epsilon_dist, ...
                Qc_model, delta_t, tau));
        end
    end
    
end

% %%
% for i = 0 : total_time_step
%     key_pos = symbol('x', i);
%     key_vel = symbol('v', i);
%     
%     % initial values: straght line
%     pose = start_conf * (total_time_step-i)/total_time_step + end_conf * i/total_time_step;
%     vel = avg_vel;
%     init_values.insert(key_pos, pose);
%     init_values.insert(key_vel, vel);
%     
%     % priors
%     if i==0
%         graph.add(PriorFactorVector(key_pos, start_conf, pose_fix));
%         graph.add(PriorFactorVector(key_vel, start_vel, vel_fix));
%     elseif i==total_time_step
%         graph.add(PriorFactorVector(key_pos, end_conf, pose_fix));
%         graph.add(PriorFactorVector(key_vel, end_vel, vel_fix));
%     end
%     
%     % GP priors and cost factor
%     if i > 0
%         key_pos1 = symbol('x', i-1);
%         key_pos2 = symbol('x', i);
%         key_vel1 = symbol('v', i-1);
%         key_vel2 = symbol('v', i);
%         graph.add(GaussianProcessPriorLinear(key_pos1, key_vel1, ...
%             key_pos2, key_vel2, delta_t, Qc_model));
%         
%         % cost factor
%         graph.add(ObstaclePlanarSDFFactorPointRobot(...
%             key_pos, pR_model, sdf, cost_sigma, epsilon_dist));
%         
%         % GP cost factor
%         if use_GP_inter & check_inter > 0
%             for j = 1:check_inter
%                 tau = j * (total_time_sec / total_check_step);
%                 graph.add(ObstaclePlanarSDFFactorGPPointRobot( ...
%                     key_pos1, key_vel1, key_pos2, key_vel2, ...
%                     pR_model, sdf, cost_sigma, epsilon_dist, ...
%                     Qc_model, delta_t, tau));
%             end
%         end
%     end
% end


%% optimize!
use_trustregion_opt = false;

if use_trustregion_opt
    parameters = DoglegParams;
    parameters.setVerbosity('ERROR');
    optimizer = DoglegOptimizer(graph, init_values, parameters);
else
    parameters = GaussNewtonParams;
    parameters.setVerbosity('ERROR');
    optimizer = GaussNewtonOptimizer(graph, init_values, parameters);
end
tic
optimizer.optimize();
toc
result = optimizer.values();
% result.print('Final results')

%% plot final graph
figure(5), hold on
title('Optimized Values')
plotEvidenceMap2D(dataset.map, dataset.origin_x, dataset.origin_y, cell_size);
for i = 1:n_E
    x1 = result.atVector(symbol('x', E(i).l.i));
    x2 = result.atVector(symbol('x', E(i).r.i));
    plot([x1(1) x2(1)], [x1(2) x2(2)], 'b');
end
hold off

%% plot final values
for i=0:total_time_step
    figure(4), hold on
    title('Optimized Values')
    % plot world
    plotEvidenceMap2D(dataset.map, dataset.origin_x, dataset.origin_y, cell_size);
    % plot arm
    conf = result.atVector(symbol('x', i));
    plotPointRobot2D(pR_model, conf);
    pause(pause_time), hold off
end
