% planar arm obstacle avoidance example, build factor graph in matlab
% @author Jing Dong
% @date Nov 16, 2015

close all
clear

import gtsam.*
import gpmp2.*


%% small dataset
dataset = generate2Ddataset('TwoObstaclesDataset');
rows = dataset.rows;
cols = dataset.cols;
cell_size = dataset.cell_size;
origin_point2 = Point2(dataset.origin_x, dataset.origin_y);

% signed distance field
field = signedDistanceField2D(dataset.map, cell_size);
sdf = PlanarSDF(origin_point2, cell_size, field);

% plot sdf
figure(2)
plotSignedDistanceField2D(field, dataset.origin_x, dataset.origin_y, dataset.cell_size);
title('Signed Distance Field')


%% settings
total_time_sec = 5.0;
total_time_step = 10;
total_check_step = 50;
delta_t = total_time_sec / total_time_step;
check_inter = total_check_step / total_time_step - 1;

% use GP interpolation
use_GP_inter = true;

% arm model
arm = generateArm('SimpleThreeLinksArm');

% GP
Qc = eye(3);
Qc_model = noiseModel.Gaussian.Covariance(Qc); 

% obstacle cost settings
cost_sigma = 0.1;
epsilon_dist = 0.1;

% prior to start/goal
pose_fix = noiseModel.Isotropic.Sigma(3, 0.0001);
vel_fix = noiseModel.Isotropic.Sigma(3, 0.0001);

% start and end conf
start_conf = [0, 0, 0]';
start_vel = [0, 0, 0]';
end_conf = [0.9, pi/2-0.9, 0]';
end_vel = [0, 0, 0]';
avg_vel = (end_conf / total_time_step) / delta_t;

% plot param
pause_time = total_time_sec / total_time_step / 5;

% plot start / end configuration
figure(1), hold on
plotEvidenceMap2D(dataset.map, dataset.origin_x, dataset.origin_y, cell_size);
plotPlanarArm(arm.fk_model(), start_conf, 'b', 2);
plotPlanarArm(arm.fk_model(), end_conf, 'r', 2);
title('Layout')
hold off

%% init graph


x1 = [start_conf ; avg_vel];
xN = [end_conf ; avg_vel];
t = delta_t * (0 : total_time_step);
n_dim = length(x1);
Qc_init = 1 * eye(n_dim);

n_branches = 10;

X = zeros([n_branches, n_dim * (total_time_step+1)]);
for i = 1:n_branches
    x = gp_cond1N(t, x1, xN, Qc_init);
    X(i,n_dim+1:end-n_dim) = x;
    X(i,1:n_dim) = x1;
    X(i,end-n_dim+1:end) = xN;
end

%% init optimization
graph = NonlinearFactorGraph;
init_values = Values;

for n = 1 : n_branches
    for i = 0 : total_time_step
        key_pos = symbol('x', n*100 + i);
        key_vel = symbol('v', n*100 + i);
        
        % initial values: straght line
%         pose = start_conf * (total_time_step-i)/total_time_step + end_conf * i/total_time_step;
%         vel = avg_vel;
        ii = i*n_dim + 1 : (i+1)*n_dim;
        ii_pos = ii(1:n_dim/2);
        ii_vel = ii(n_dim/2+1:end);
        init_values.insert(key_pos, X(n, ii_pos)');
        init_values.insert(key_vel, X(n, ii_vel)');
%         init_values.insert(key_pos, pose);
%         init_values.insert(key_vel, vel);

        % priors
        if i==0
            graph.add(PriorFactorVector(key_pos, start_conf, pose_fix));
            graph.add(PriorFactorVector(key_vel, start_vel, vel_fix));
        elseif i==total_time_step
            graph.add(PriorFactorVector(key_pos, end_conf, pose_fix));
            graph.add(PriorFactorVector(key_vel, end_vel, vel_fix));
        end
        
        % GP priors and cost factor
        if i > 0
            key_pos1 = symbol('x', n*100 + i-1);
            key_pos2 = symbol('x', n*100 + i);
            key_vel1 = symbol('v', n*100 + i-1);
            key_vel2 = symbol('v', n*100 + i);
            graph.add(GaussianProcessPriorLinear(key_pos1, key_vel1, ...
                key_pos2, key_vel2, delta_t, Qc_model));
            
            % cost factor
            graph.add(ObstaclePlanarSDFFactorArm(...
                key_pos, arm, sdf, cost_sigma, epsilon_dist));
            
            % GP cost factor
            if use_GP_inter & check_inter > 0
                for j = 1:check_inter
                    tau = j * (total_time_sec / total_check_step);
                    graph.add(ObstaclePlanarSDFFactorGPArm( ...
                        key_pos1, key_vel1, key_pos2, key_vel2, ...
                        arm, sdf, cost_sigma, epsilon_dist, ...
                        Qc_model, delta_t, tau));
                end
            end
        end
    end
end

% %% plot initial values
for n = 1 : n_branches
    for i=0:total_time_step
        figure(3), hold on
        title('Initial Values')
        % plot world
        plotEvidenceMap2D(dataset.map, dataset.origin_x, dataset.origin_y, cell_size);
        % plot arm
        conf = init_values.atVector(symbol('x', n*100 + i));
        plotPlanarArm(arm.fk_model(), conf, 'b', 2);
        pause(pause_time), hold off
    end
end

%% optimize!
use_trustregion_opt = false;

if use_trustregion_opt
    parameters = DoglegParams;
    parameters.setVerbosity('ERROR');
    optimizer = DoglegOptimizer(graph, init_values, parameters);
else
    parameters = GaussNewtonParams;
    parameters.setVerbosity('ERROR');
    optimizer = GaussNewtonOptimizer(graph, init_values, parameters);
end
tic
optimizer.optimize();
toc
result = optimizer.values();
% result.print('Final results')

%% plot final values
for n = 1:n_branches
    for i=0:total_time_step
        figure(4), hold on
        title('Optimized Values')
        % plot world
        plotEvidenceMap2D(dataset.map, dataset.origin_x, dataset.origin_y, cell_size);
        % plot arm
        conf = result.atVector(symbol('x', n*100 + i));
        plotPlanarArm(arm.fk_model(), conf, 'b', 2);
        pause(pause_time), hold off
    end
end
