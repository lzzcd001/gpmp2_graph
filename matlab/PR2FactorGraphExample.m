% @brief    7DOF PR2 right arm example for GP Simultaneous Planning and Control
% @author   Mustafa Mukadam
% @date     August 24, 2016

%% Load libraries
import gtsam.*
import gpmp2.*
clear
clc

%% Settings
delta_t = 0.05; % time between two support states
time_horizon = 1; % receding horizon
intp_states = 10; % number of interpolation states

Qx = 0.01;
Qu = 100;
Qfix = 1e-4;
Qgoal = 0.01;
Qm = 0.01;

Qobs = 0.01;
epsilon_dist = 0.2;


seed = 0;
maxiter = 100;

save_traj = 0;

%% Video output
output_video = false;
result_video_filename = '~/Desktop/result.avi';
if output_video
    vwr = VideoWriter(result_video_filename, 'Motion JPEG AVI');
    vwr.Quality = 100;
    vwr.FrameRate = round(intp_states/delta_t);
    open(vwr);
end

%% Robot and Dynamics setup
%-------------Trajectory time and number of states
params.support_states = time_horizon/delta_t;

%-------------Arm model
params.ndim = 7;
params.dim = 2*params.ndim;
alpha = [-1.5708, 1.5708, -1.5708, 1.5708, -1.5708, 1.5708, 0]';
a = [0.1, 0, 0, 0, 0, 0, 0]';
d = [0, 0, 0.4, 0, 0.321, 0, 0]';
theta = [0, 1.5708, 0, 0, 0, 0, 0]';
arm_origin = [-0.18053, -0.0441087, 0.958925]';
base_pose = Pose3(Rot3([0.646842  0.762624   0;
                       -0.762624  0.646842  -0;
                       -0         0          1]), Point3(arm_origin));
arm = Arm(7, a, alpha, d, base_pose, theta);
spheres_data = [...
      0 -0.010000 0.000000 0.000000 0.180000

      2 0.015000 0.220000 -0.000000 0.110000
      2 0.035000 0.140000 -0.000000 0.080000
      2 0.035000 0.072500 -0.000000 0.080000
      2 0.000000 0.000000 -0.000000 0.105000

      4 -0.005000 0.321-0.130000 -0.000000 0.075000
      4 0.010000 0.321-0.200000 -0.025000 0.055000
      4 0.010000 0.321-0.200000 0.025000 0.055000
      4 0.015000 0.321-0.265000 -0.027500 0.050000
      4 0.015000 0.321-0.265000 0.027500 0.050000
      4 0.005000 0.321-0.320000 -0.022500 0.050000
      4 0.005000 0.321-0.320000 0.022500 0.050000

      6 0 -0.017500 0.072500 0.040000
      6 0 0.017500 0.072500 0.040000
      6 0 0 0.092500 0.040000

      6 0 0.03600 0.11 0.040000
      6 0 0.027000 0.155 0.035000
      6 0 0.00900 0.18 0.030000
      6 0 0.00950 0.205 0.020000

      6 0 -0.03600 0.11 0.040000
      6 0 -0.027000 0.155 0.035000
      6 0 -0.00900 0.18 0.030000
      6 0 -0.00950 0.205 0.020000];
nr_body = size(spheres_data, 1);
sphere_vec = BodySphereVector;
for i=1:nr_body
    sphere_vec.push_back(BodySphere(spheres_data(i,1), spheres_data(i,5), ...
        Point3(spheres_data(i,2:4)')));
end
arm = ArmModel(arm, sphere_vec);

% %-------------State, Control and Dynamics
% e = eye(params.ndim); z = zeros(params.ndim);
% Aq = [z, e; z, z];
% Bq = [z; e];
% Au = [z];
% Fq = [z; e];
% Fu = [e];
% A = [Aq, Bq; z, z, Au];
% F = blkdiag(Fq, Fu);
% Kwx = Qx * eye(params.ndim);
% Kwu = Qu * eye(params.ndim);
% W = blkdiag(Kwx, Kwu);
% R = Qm*eye(params.ndim);

% Phi = expm(A*delta_t);
% fun = @(s) expm(A*s)*F*W*F'*expm(A*s)';
% Q = integral(fun, 0, delta_t, 'ArrayValued', 1);
% mu = zeros(params.dim,1);

% %-------------GP interpolation
% if intp_states
%     [Lambda, Psi] = getLambdaAndPsi(A,F,W,delta_t,intp_states);
% end

%% Initializations
rng(seed);
fix_cov = noiseModel.Isotropic.Sigma(params.ndim, Qfix);
goal_cov = noiseModel.Isotropic.Sigma(params.ndim, Qgoal);


states{1} = [-0.2762,  0.894 , -0.4462, -1.6105,  2.328 , -1.0881, -2.3999]';
states{2} = [-0.571 , -0.3534, -1.7684, -1.5384, -2.7693, -1.5537, -1.4818]';
states{3} = [-0.1708,  0.093 , -1.1193, -1.0388,  2.8681, -1.4571, -1.9924]';
states{4} = [ 0.2855, -0.198 , -3.2144, -1.0791, -2.0565, -1.1219, -0.6414]';
states{5} = [-0.5471,  0.1817, -1.835 , -1.6187,  3.0026, -1.7675, -1.3979]';
states{6} = [ 0.1713,  0.377 , -0.5826, -0.5771, -1.5264, -0.4143,  2.0764]';
states{7} = [ 0.514 ,  0.2662, -1.2524, -0.6177,  2.9156, -0.2591, -1.7356]';
states{8} = [ 0.5512, -0.3535, -1.2124, -0.4724, -2.1021, -0.5965, -2.8023]';
states{9} = [ 0.5272,  0.7193, -0.9876, -0.5453,  1.2938, -0.3151, -0.5195]';
states{10} = [-0.3728,  1.0259, -0.9876, -1.2082,  2.0042, -1.3781, -1.6173]';


count = 0;
for start_no = 1:10
  for end_no = start_no+1:10
    start = states{start_no};
    goal = states{end_no};

    % start = [-0.2762, 0.894 , -0.4462, -1.6105, 2.328 , -1.0881, -2.3999]';
    % goal =  [-0.571 , -0.3534, -1.7684, -1.5384, -2.7693, -1.5537, -1.4818]';

    curr_robot_pos = start;
    crp_ws = arm.fk_model().forwardKinematicsPosition(curr_robot_pos);
    crp_ws = crp_ws(:,end);
    g_ws = arm.fk_model().forwardKinematicsPosition(goal);
    g_ws = g_ws(:,end);
    s_ws = crp_ws;
    curr_time = 0;
    curr_step = 0;

    init_vel = zeros(params.ndim,1);

    % env
    origin = [-1.2, -1.2, -1.2]' + arm_origin;
    cell_size = 0.02;
    origin_point3 = Point3(origin);
    field_raw = load('sdf/industrial2.txt');
    field_x = 120;
    field_y = 120;
    field_z = 120;
    field = [];
    for i = 1:field_z
        field(:,:,i) = [field_raw((i-1)*field_x+1:i*field_x, :)];
    end
    sdf = SignedDistanceField(origin_point3, cell_size, size(field, 1), ...
        size(field, 2), size(field, 3));
    for z = 1:size(field, 3)
        sdf.initFieldData(z-1, field(:,:,z));
    end

    % % Plot env setup
    % h = figure(1); clf(1);
    % set(h, 'Position', [-1200, 100, 1100, 1200]);
    % hold on; axis off;
    % plotSignedDistanceField3D(field, origin, cell_size);
    % % hcp = plotRobotModel(arm, start);
    % plotArm(arm.fk_model(), goal, 'r', 2);
    % view(-69,24);
    % zoom(1.5);
    % hold off
    % pause(0.001);

    % if save_traj
    %     delete('traj/*');
    % end



    % GP
    Qc = 4 * eye(7);
    Qc_model = noiseModel.Gaussian.Covariance(Qc);
    % time steps
    total_time_sec = 5.0;
    total_time_step = 10;
    total_check_step = 50;
    delta_t = total_time_sec / total_time_step;
    % prior to start/goal
    pose_fix = fix_cov;
    vel_fix = goal_cov;

    % start and end conf
    % start_conf = [-18, -8]';
    start_conf = start;
    start_vel = zeros(params.ndim, 1);
    % end_conf = [18, 15]';
    end_conf = goal;
    end_vel = zeros(params.ndim, 1);
    avg_vel = (end_conf / total_time_step) / delta_t;

    % use GP interpolation
    use_GP_inter = true;
    % obstacle cost settings
    cost_sigma = 0.3;
    epsilon_dist = 1.5;

    % gp struct
    gp = struct;
    gp.t = linspace(0, total_time_sec, total_time_step);
    gp.Qc_model = Qc_model;
    gp.pose_fix = pose_fix;
    gp.vel_fix = vel_fix;
    gp.x0 = start_conf;
    gp.xN = end_conf;
    gp.vel = avg_vel;
    gp.use_GP_inter = use_GP_inter;
    gp.total_time_sec = total_time_sec;
    gp.total_time_step = total_time_step;
    gp.total_check_step = total_check_step;
    gp.delta_t = total_time_sec / total_time_step;
    gp.check_inter = total_check_step / total_time_step - 1;
    gp.cost_sigma = cost_sigma;
    gp.epsilon_dist = epsilon_dist;

    display('')
    display('Generating graph...')
    G = generateRandomBranchedGraph(30, 5, gp);

    % params
    Qc = 10 * eye(7);
    gp.Qc_model = noiseModel.Gaussian.Covariance(Qc);
    gp.cost_sigma = 0.2;
    gp.epsilon_dist = 2.0;

    dataset.sdf = sdf;


    robot = struct;
    robot.pR_model = arm;
    % Initialize factors.
    display('')
    display('Converting to factor graph...')
    [graph, init_values] = convertToFactorGraphArm(G, gp, robot, dataset);
    % optimize!
    display('')
    display('Optimizing factor graph...')
    [result, elapsedTime] = gtsamOptimize(graph, init_values);
    G = copyResultsToGraph(G, result);

    % Get MAP trajectory.
    display('')
    display('Retrieving MAP trajectory...')
    [H, total_cost] = getMAPTrajectoryArm(G, graph, result);

    collisionFree = checkCollisionsArm(H, sdf, robot.pR_model, Qc_model, dataset)

    refineMAP = true;
    % Refine MAP trajectory.
    display('')
    display('Refining MAP trajectory...')
    if refineMAP
        [graph, init_values] = convertToFactorGraphArm(H, gp, robot, dataset);
        [result, elapsedTime] = gtsamOptimize(graph, init_values);
        H = copyResultsToGraph(H, result);
    end
    [H, total_cost] = getMAPTrajectoryArm(H, graph, result);
    collisionFree = checkCollisionsArm(H, sdf, robot.pR_model, Qc_model, dataset)

    % Check for collisions.
    tic;
    collisionFree = checkCollisionsArm(G, sdf, robot.pR_model, Qc_model, dataset);
    collisionCheckTime = toc;

    if collisionFree == 1
      count = count + 1;
    end

    Mu = H.Nodes.Pos;
    [count, start_no, end_no]
  end
end

accuracy = count / 45.0;


    % %-------simulate
    % [Mu, K] = getPosterior(graph,result,params);
    % delta_t_rp = delta_t/(intp_states+1);
    % Phi_rp = expm(A*delta_t_rp);
    % Int_rp = integral(fun, 0, delta_t_rp, 'ArrayValued', 1);
    % Mu_0 = Mu(1:params.dim);
    % K_0 = K(1:params.dim,1:params.dim);
    % gp = GPInterpolator(1, delta_t, params.ndim, intp_states, A, F, W);
    % gp.interpolate(Mu(1:2*params.dim));
    % planner = Replanner(params.ndim, gp, R, Mu(1:2*params.dim), K(1:2*params.dim,1:2*params.dim));
    % %planner = gp;
    % Mu2 = simulate(1, intp_states, params.ndim, Phi_rp, Int_rp, R, planner, Mu_0, K_0, true);
    % %Mu2 = gp.V_';
    
%     %-------------Plot env and trajectory
%     figure(1);
%     hold on;
%     pos_all = [];
%     for i=2:size(Mu,2)
%         plotArm(arm.fk_model(), Mu(1:params.ndim,i), 'b', 2);
%         if save_traj
%             pos = arm.fk_model().forwardKinematicsPosition(Mu(1:params.ndim,i));
%             pos_all = [pos_all pos(:,end)];
%         end
%     end
%     if save_traj
%         dlmwrite(['traj/', num2str(iter), '.txt'],pos_all','delimiter','\t','precision','%.3f');
%     end
%     hold off;
%     past_traj = [];
%     for i=2:size(Mu,2)
%         curr_robot_pos = Mu(1:params.ndim,i);
%         figure(1);
%         hold on;
%         % delete(hcp); hcp = plotArm(arm.fk_model(), curr_robot_pos, 'b', 2);
%         plotRobotModel(arm, curr_robot_pos);
%         hold off;
%         if output_video
%             currFrame = getframe(h);
%             writeVideo(vwr, currFrame);
%         end
%         past_traj = [past_traj curr_robot_pos];
%         pause(0.001);
%     end
    
%     crp_ws = arm.fk_model().forwardKinematicsPosition(curr_robot_pos);
%     crp_ws = crp_ws(:,end);

%     if (norm(crp_ws - g_ws) < 1e-2)
%         disp('DONE');
%         assert(false)
%     end

% %%
% if save_traj
%     dlmwrite(['traj/mpc.txt'],past_traj','delimiter','\t','precision','%.3f');
% end
% if output_video
%     close(vwr);
% end