%% Generate symbolic netgraph covariances.
clc;
clear;

N = 4;

% Generate Q matrix.
Q = sym('P0');
for i = 1:N
    Q = [Q sym(['Q' num2str(i)], 'real')];
end
Q = diag(Q);
Q_inv = inv(Q);

% Generate F inverse matrix.
F_inv = sym(eye(N + 1));
for i = 0:N-1
    F_inv(i+2,i+1) = -sym(['phi' num2str(i+1) '_' num2str(i)], 'real');
end
F_inv;

size(F_inv);
size(Q_inv);

P_inv = F_inv' * Q_inv * F_inv
P = inv(P_inv);

%% 
PP_inv = [P_inv sym(zeros(N+1)); sym(zeros(N+1)) P_inv];
PP = inv(PP_inv)

PP_inv(N+2,1) = sym('x', 'real')
PP_inv(1,N+2) = sym('x', 'real')

PP = inv(PP_inv)