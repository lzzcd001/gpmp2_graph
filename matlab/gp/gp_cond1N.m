function [ x, K_cond ] = gp_cond1N( t, x1, xN, Qc, P0 )
%GP_COND 

%% Setup the GP prior.

% Get the number of dimensions.
n_dim = length(x1);

% Create the LTV-SDE transition matrix.
n_states = n_dim / 2; % HACK
phi = @(t, tau) [ eye(n_states), (t - tau) * eye(n_states);
                zeros(n_states),             eye(n_states) ];

% L = [0; 0; 1; 1];
L = eye(n_dim);
L(1:n_states,1:n_states) = 0;

% Initialize a linear mean function.
x_bar = xbar_linear(t, x1, xN, phi);

% Compute lifted transition matrix and its inverse.
F_ = F(t, phi);
F_inv = Finv(t, phi);

% Compute lifted covariance matrix.
Q_ = Q(t, phi, L, Qc, P0);
Q_inv = Qinv(Q_, n_dim);
P = F_ * Q_ * F_';
P_inv = F_inv' * Q_inv * F_inv;


%% Sample from GP posterior.

% Get covariance matrix.
P_bar = P;

% Decompose covariance matrix into training/test covariances. We're
% conditioning on the first and last timesteps.
n = length(P_bar);
ii = [1:n_dim n-n_dim+1:n n_dim+1:n-n_dim];
P_bar = P_bar(ii, ii);

% Sample from posterior.
ii_tr = [1:n_dim*2];
ii_ts = [n_dim*2+1:n];
K_tr = P_bar(ii_tr, ii_tr);
K_ts = P_bar(ii_ts, ii_ts);
K_trts = P_bar(ii_tr, ii_ts);
K_tstr = P_bar(ii_ts, ii_tr);

f = K_tstr * inv(K_tr) * [x1 ; xN];
K_cond = K_ts - K_tstr * inv(K_tr) * K_trts;
K_cond = (K_cond + K_cond') / 2;

% plot(f(1:4:end))
% hold on

x_ts = mvnrnd(f, K_cond);
% plot(x_ts(1:4:end))
x = x_ts;
x = f;

end

