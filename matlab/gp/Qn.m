function [ Qnval ] = Qn( t1, t0, phi, L, Qc )
%QN

%%
% integrand = @(tau) phi(t1, tau) * L * Qc * L' * phi(t1, tau)';
% Qnval = integral(integrand, t0, t1, 'ArrayValued', true);

% Get dimensions.

% Assume we use LTV-SDE GP.
dt = t1 - t0;
Qnval = [1/3*dt^3*Qc, 1/2*dt^2*Qc; 1/2*dt^2*Qc dt*Qc];

end

