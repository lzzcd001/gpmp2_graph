function [ F_inv ] = Finv( t, phi )
%FINV 

%%
N = length(t);
n_phi = length(phi(0,0));
n_F = N * n_phi;
F_inv = eye(n_F);
for j = 1:N-1
    i = j + 1;
    F_inv((i-1)*n_phi+1:i*n_phi,(j-1)*n_phi+1:j*n_phi) = ...
        -phi(t(i), t(j));
end

end

