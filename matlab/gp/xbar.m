function [ x_bar ] = xbar( tau, t, x0, phi )
%XBAR

%%
x_bar = phi(tau, t(1)) * x0;

end

