%% Generate net.

% The number of timesteps to use.
n_timesteps = 5;
t = linspace(0, 1, n_timesteps);

% The number of neighbors at each node.
n_neighbors = 3;

% The number of nodes at each timestep.
n_branches = 5;

% Create the graph transition matrix.
n_nodes = 2 + (n_timesteps - 2) * n_branches;
A = zeros(n_nodes);
for i = 1 : n_timesteps - 1
    for v = 1 : n_branches
        
        % 
        if i == 1 || i == n_timesteps - 1
            A(i,v) = 1;
        end
        
        % 
        
        
    end
end