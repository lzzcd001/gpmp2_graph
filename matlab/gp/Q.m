function [ Qval ] = Q( t, phi, L, Qc, P0 )
%Q 

%%
n_dim = length(phi(0,0));
N = length(t);
Qval = zeros(n_dim * N);
Qval(1:n_dim, 1:n_dim) = P0;
for i = 2:N
    b = (i-1)*n_dim+1:i*n_dim;
    Qval(b,b) = Qn(t(i), t(i-1), phi, L, Qc);
end

end

