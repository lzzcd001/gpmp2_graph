%% LTV-SDE prior.

% Initial conditions.
n_states = 1;
t = linspace(0, 1, 100);
% x1 = [0 0 0 0]';
x1 = [0 0]';
% xN = [1 1 0 0]';
xN = [pi/2 0]';

% Initialize GP prior.
gp.n_dim = 2 * n_states;
gp.t = t;
gp.phi = @(t, tau) [ eye(n_states), (t - tau) * eye(n_states);
                           zeros(n_states), eye(n_states) ];
% gp.L = eye(gp.n_dim);
% gp.L(1:n_states, 1:n_states) = 1;
% gp.L(n_states+1:end, n_states+1:end) = 0.1;

% gp.L(1,1) = 0;
% gp.L(1,2) = 1;
% gp.L(2,2) = 1;
gp.L = [0; 1];

gp.Qc = 100; % * eye(gp.n_dim);
gp.P0 = 0.01 * eye(gp.n_dim);

% Initialize a linear mean function.
gp.x_bar = xbar_linear(gp.t, x1, xN, gp.phi);

% Compute lifted transition matrix and its inverse.
gp.F = F(gp.t, gp.phi);
gp.F_inv = Finv(gp.t, gp.phi);

% Compute lifted covariance matrix.
gp.Q = Q(gp.t, gp.phi, gp.L, gp.Qc, gp.P0);
gp.Q_inv = Qinv(gp.Q, gp.n_dim);
gp.P = gp.F * gp.Q * gp.F';
gp.P_inv = gp.F_inv' * gp.Q_inv * gp.F_inv;


%% Sample from GP posterior.

% Get number of dimensions.
n_dim = gp.n_dim;

% Get covariance matrix.
P_bar = gp.P;

% Decompose covariance matrix into training/test covariances. We're
% conditioning on the first and last timesteps.
n = length(P_bar);
ii = [1:n_dim n-n_dim+1:n n_dim+1:n-n_dim];
P_bar = P_bar(ii, ii);

% Sample from posterior.
ii_tr = [1:n_dim*2];
ii_ts = [n_dim*2+1:n];
K_tr = P_bar(ii_tr, ii_tr);
K_ts = P_bar(ii_ts, ii_ts);
K_trts = P_bar(ii_tr, ii_ts);
K_tstr = P_bar(ii_ts, ii_tr);

f = K_tstr * inv(K_tr) * [x1 ; xN];
K_cond = K_ts - K_tstr * inv(K_tr) * K_trts;
K_cond = (K_cond + K_cond') / 2;

plot(f(1:2:end))
hold on

x_ts = mvnrnd(f, K_cond);
plot(x_ts(1:2:end))
