%% LTV-SDE prior.

% Initial conditions.
n_states = 1;
t = linspace(0, 1, 10);
% x1 = [0 0 0 0]';
x1 = [0 0]';
% xN = [1 1 0 0]';
xN = [1 0]';

% Initialize GP prior.
gp.n_dim = 2 * n_states;
gp.t = t;
gp.phi = @(t, tau) [ eye(n_states), (t - tau) * eye(n_states);
                           zeros(n_states), eye(n_states) ];
% gp.L = eye(gp.n_dim);
% gp.L(1:n_states, 1:n_states) = 1;
% gp.L(n_states+1:end, n_states+1:end) = 0.1;

% gp.L(1,1) = 0;
% gp.L(1,2) = 1;
% gp.L(2,2) = 1;
gp.L = [0; 1];

gp.Qc = 1; % * eye(gp.n_dim);
gp.P0 = 0.01 * eye(gp.n_dim);

% Initialize a linear mean function.
gp.x_bar = xbar_linear(gp.t, x1, xN, gp.phi);

% Compute lifted transition matrix and its inverse.
gp.F = F(gp.t, gp.phi);
gp.F_inv = Finv(gp.t, gp.phi);

% Compute lifted covariance matrix.
gp.Q = Q(gp.t, gp.phi, gp.L, gp.Qc, gp.P0);
gp.Q_inv = Qinv(gp.Q, gp.n_dim);
gp.P = gp.F * gp.Q * gp.F';
gp.P_inv = gp.F_inv' * gp.Q_inv * gp.F_inv;


%% Sample from LTV-SDE prior.

% Add sigma to ensure PSD of the covariance matrix.
sigma = 1e-7 * eye(length(gp.P));

% Generate samples.
gp.x_hat = mvnrnd(gp.x_bar, gp.P + sigma)';

% Plot mean positions.
subplot(211)
hold on
plot(gp.t, gp.x_bar(1:gp.n_dim:end));
plot(gp.t, gp.x_hat(1:gp.n_dim:end), 'r-');
grid on

% Plot mean velocities.
subplot(212)
hold on
plot(gp.t, gp.x_bar(2:gp.n_dim:end));
plot(gp.t, gp.x_hat(2:gp.n_dim:end), 'r-');
grid on


%% Mean and covariance of LTV-SDE posterior.

% Initialize interpolation.
n_tau = 1000;
tau = linspace(0.0, 1.1, n_tau);
x_tau = zeros([gp.n_dim * n_tau, 1]);
P_tau = zeros([gp.n_dim * n_tau, 1]); % Diagonal terms.

% Get function references.
Q_tau = @(tau, t_n) Qtau(tau, t_n, gp.phi, gp.L, gp.Qc);
Q_n = @(t1, t0) Qn(t1, t0, gp.phi, gp.L, gp.Qc);

for i = 1:n_tau
    b = (i-1)*gp.n_dim+1:i*gp.n_dim;
    x_tau(b) = xtau(tau(i), gp.t, gp.x_bar, gp.x_hat, gp.phi, Q_n, Q_tau);
end

%% Plot posterior mean position.
subplot(211)
hold on
plot(tau, x_tau(1:gp.n_dim:end))
plot(gp.t, gp.x_hat(1:gp.n_dim:end), 'rx')
plot(gp.t, gp.x_bar(1:gp.n_dim:end))
grid on

%% Plot posterior mean velocity.
subplot(212)
hold on
plot(tau, x_tau(2:gp.n_dim:end))
plot(gp.t, gp.x_hat(2:gp.n_dim:end), 'rx')
plot(gp.t, gp.x_bar(2:gp.n_dim:end))
grid on

%% Symbolic solutions of LTV-SDE.

% Qn
phi = gp.phi;
L = gp.L;
Qc = gp.Qc;
tau = sym('tau');
t0 = sym('t0');
t1 = sym('t1');
dt = sym('dt');
s = sym('s');
integrand = phi(t1, s) * L * Qc * L' * phi(t1, s)';
Q_n = int(integrand, s, t0, t1)

% Qn^-1
Qn_inv = simplify(inv(Q_n))

% Qtau
qtau_integrand = phi(tau, s) * L * Qc * L' * phi(tau, s)';
Q_tau = int(qtau_integrand, s, t0, tau)

%
P = simplify(Q_tau * phi(t1, tau)' * Qn_inv)
L = simplify(phi(tau, t0) - P * phi(t1, t0))

% Plot P, L
num_pts = 100;
t_i = linspace(1e-7, 1, num_pts);
P_vec = zeros([num_pts, size(P)]);
L_vec = zeros([num_pts, size(L)]);
for i = 1:num_pts
    sub_syms = [t0, t1, tau];
    sub_vals = [0, 1, t_i(i)];
    P_vec(i,:,:) = subs(P, sub_syms, sub_vals);
    L_vec(i,:,:) = subs(L, sub_syms, sub_vals);
end

subplot(231)
hold on
plot(t_i, L_vec(:,1,1)+L_vec(:,1,2), 'b')
plot(t_i, P_vec(:,1,1)+P_vec(:,1,2), 'g')
plot(t_i, L_vec(:,1,1)+L_vec(:,1,2) + P_vec(:,1,1)+P_vec(:,1,2), 'r')
legend('\Lambda', '\Psi')
subplot(232)
hold on
plot(t_i, L_vec(:,1,1), 'b')
plot(t_i, P_vec(:,1,1), 'g')
legend('\Lambda', '\Psi')
subplot(233)
hold on
plot(t_i, L_vec(:,1,2), 'b')
plot(t_i, P_vec(:,1,2), 'g')
legend('\Lambda', '\Psi')
subplot(234)
hold on
plot(t_i, L_vec(:,2,1)+L_vec(:,2,2), 'b')
plot(t_i, P_vec(:,2,1)+L_vec(:,2,2), 'g')
plot(t_i, L_vec(:,2,1)+L_vec(:,2,2) + P_vec(:,2,1)+P_vec(:,2,2), 'r')
legend('\Lambda', '\Psi')
subplot(235)
hold on
plot(t_i, L_vec(:,2,1), 'b')
plot(t_i, P_vec(:,2,1), 'g')
legend('\Lambda', '\Psi')
subplot(236)
hold on
plot(t_i, L_vec(:,2,2), 'b')
plot(t_i, P_vec(:,2,2), 'g')
legend('\Lambda', '\Psi')

