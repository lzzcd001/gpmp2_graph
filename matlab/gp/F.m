function [ Fval ] = F( t, phi )
%F 

%%
N = length(t);
n_phi = length(phi(0,0));
n_F = N * n_phi;
Fval = eye(n_F);
for j = 1:N
    for i = j+1:N
        Fval((i-1)*n_phi+1:i*n_phi,(j-1)*n_phi+1:j*n_phi) = ...
            phi(t(i), t(j));
    end
end

end

