function [ x_tau ] = xtau( tau, t, x_bar, x_hat, phi, Q_n, Q_tau )
%XTAU 

%%

% Find n such that t_n <= tau <= t_n+1.
n_dim = length(phi(0,0));
N = length(t);
n = find(t <= tau, 1, 'last');

if isempty(n)
    n = 1;
end


% Interpolate the value of x_hat(tau).

nn = (n-1)*n_dim+1:n*n_dim;
x_b = phi(tau, t(n)) * x_bar(nn);

if n == N
    x_tau = x_b + phi(tau, t(n)) * (x_hat(nn) - x_b);
else

    Psi = Q_tau(tau, t(n)) * phi(t(n+1), tau)' * inv(Q_n(t(n+1), t(n)));
    
    Lambda = phi(tau, t(n)) - Psi * phi(t(n+1), t(n));

    x_tau = x_b + [Lambda, Psi] * [ x_hat(nn) - x_b; x_hat(nn+n_dim) - phi(tau, t(n+1))*x_bar(nn+n_dim) ];
end

end