function [ Q_inv ] = Qinv( Q, n_dim )
%QINV

%%
N = length(Q) / n_dim;
Q_inv = zeros(size(Q));
for i = 1:N
    b = (i-1)*n_dim+1:i*n_dim;
    Q_inv(b,b) = inv(Q(b,b));
end

end

