function [ Q_tau ] = Qtau( tau, tn, phi, L, Qc )
%QTAU 

%%
integrand = @(s) phi(tau, s) * L * Qc * L' * phi(tau, s)';
Q_tau = integral(integrand, tn, tau, 'ArrayValued', true);

end

