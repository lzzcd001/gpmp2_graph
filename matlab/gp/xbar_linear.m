function [ x_bar ] = xbar_linear( t, x1, xN, phi )
%XBAR_LINEAR 

%%
n_dim = length(x1);
n_ts = length(t);
x_bar = zeros([n_dim * n_ts, 1]);

% Compute linear velocity term.
v = (xN(1:n_dim/2) - x1(1:n_dim/2)) / (t(n_ts) - t(1));
x1(n_dim/2+1:end) = v;

% Compute x bar.
for i = 1:n_ts
    b = (i-1)*n_dim+1:i*n_dim;
    x_bar(b) = phi(t(i), t(1)) * x1;
end

end

